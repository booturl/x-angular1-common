if (!utilityModule)
    var utilityModule = angular.module("app.service.utility", []);
/**
 * 工具类
 */
utilityModule.factory('utilService', ['$http', '$rootScope', function ($http, $rootScope) {
    var utilService = {};
    /**
     * @Title 判断空
     * @Description 判断空
     * @author hedongyang
     * @param val
     * @returns {Boolean} 空返回true
     */
    utilService.isEmpty = function (val) {
        val = $.trim(val);
        if (val == null)
            return true;
        if (val == undefined || val == 'undefined')
            return true;
        if (val == "")
            return true;
        if (val.length == 0)
            return true;
        if (!/[^(^\s*)|(\s*$)]/.test(val))
            return true;
        return false;
    };
    /**
     * @Title 判断非空
     * @Description 判断非空
     * @author hedongyang
     * @param val
     * @returns {Boolean} 非空返回true
     */
    utilService.isNotEmpty = function (val) {
        return !utilService.isEmpty(val);
    };
    /**
     * @Title 将小数转换为百分比
     * @Description 将小数转换为百分比
     * @author hedongyang
     * @param val,n
     * @returns {Boolean}  空返回true
     */
    utilService.getPercent = function (val, n) {
        if (utilService.isEmpty(val))
            return "0%";
        n = n || 2;
        return ( Math.round(val * Math.pow(10, n + 2)) / Math.pow(10, n) ).toFixed(n) + '%';
        //return val.slice(2,4)+"."+val.slice(4,6)+"%";
    };
    /**
     * @Title  深度拷贝
     * @Description 深度拷贝
     * @author 'network'
     * @version V1.0
     */
    utilService.extendDeep = function (dst) {
        angular.forEach(arguments, function (obj) {
            if (obj !== dst) {
                angular.forEach(obj, function (value, key) {
                    if (angular.isObject(dst[key]) || angular.isArray(dst[key])) {
                        utilService.extendDeep(dst[key], value);
                    } else {
                        dst[key] = angular.copy(value);
                    }
                });
            }
        });
        return dst;
    };
    /**
     * @Title 返回第一时间
     * @Description 返回第一时间
     * @author hedongyang
     * @param date 进行加减的日期，格式YYYY-MM-DD
     * @param num 往前算就传入负数，往后算就传入正数
     * @param type 月加减和日加减
     * @returns {*}
     */
    utilService.getFirstDate = function(date,num,type){
        var nowDate = new Date();
        if(!num){
            num = 0;
        }
        if(!date){
            date = nowDate;
        }
        if(!type){
            type = "dd";
        }
        var d = nowDate;
        if(type == "mm"  || type == "MM"){
            //月加减
            d.setMonth(d.getMonth()+num);
            var month=d.getMonth();
            if(month<10){
                month = "0"+month;
            }
            var val = d.getFullYear()+"-"+month;
            return new Date(d.getFullYear(), month);
        }
        if(type == "dd" || type == "DD"){
            //日加减
            d.setDate(d.getDate()+num);
            var month=d.getMonth();
            var day = d.getDate();
            if(month<10){
                month = "0"+month;
            }
            if(day<10){
                day = "0"+day;
            }
            var val = d.getFullYear()+"-"+month+"-"+day;
            return new Date(d.getFullYear(), month,day);
        }
    }

    /**
     * @Title 返回对应时间精度的第一时刻
     * @author hedongyang
     * @param str 日期字符串
     * @param type 日期精度
     * @returns {Date}
     */
    utilService.strToDate = function(str,type){
        if(!str){
            str = ""+new Date();
        }
        var beginArr = str.split("-");
        if(type == "mm" || type == "MM"){
            //返回每月的第一时刻
            return new Date(beginArr[0], beginArr[1]-1);
        }
        if(type == "dd" || type == "DD"){
            //返回每天的第一时刻
            return new Date(beginArr[0], beginArr[1]-1, beginArr[2].substr(0,2));
        }
    }
    /**
     * 根据所提供的原始时间和增长幅度来获取新的时间（字符串形式）
     * range——增加的幅度（int），可以是负数
     * originDate—— 原始时间，如果不传，则默认是new Date();originDate可以是字符串型‘2015-05-01’，也可以是long型
     */
    utilService.getDate = function (range, originDateStr) {
        var originDate = new Date();
        if (originDateStr) {
            originDate = new Date(originDateStr);
        }
        if (typeof range !== 'number' || !range) {
            range = 0;
        }
        return new Date(originDate.getTime() + 24 * 60 * 60 * 1000 * range);
    };

    utilService.getIntegralDate = function (range, originDateStr) {
        var originDate = new Date();
        if (originDateStr) {
            originDate = new Date(originDateStr);
        }
        if (typeof range !== 'number' || !range) {
            range = 0;
        }
        var time = originDate.getTime() + 24 * 60 * 60 * 1000 * range;
        var integralTime = parseInt(time / (24 * 60 * 60 * 1000));
        return new Date(integralTime * 24 * 60 * 60 * 1000 - 8 * 3600 * 1000);
    };

    utilService.getDay = function (range, originDateStr) {
        var originDate = new Date();
        if (originDateStr) {
            originDate = new Date(originDateStr);
        }
        if (typeof range !== 'number' || !range) {
            range = 0;
        }
        var date = new Date(originDate.getTime() + 24 * 60 * 60 * 1000 * range);
        return date.getDate();
    };

    utilService.getMonth = function (range, originDateStr) {
        var originDate = new Date();
        if (originDateStr) {
            originDate = new Date(originDateStr);
        }
        if (typeof range !== 'number' || !range) {
            range = 0;
        }
        originDate.setMonth(originDate.getMonth() + range);
        return originDate.getMonth() + 1;
    };

    utilService.getYear = function (range, originDateStr) {
        var originDate = new Date();
        if (originDateStr) {
            originDate = new Date(originDateStr);
        }
        if (typeof range !== 'number' || !range) {
            range = 0;
        }
        originDate.setFullYear(originDate.getFullYear() + range);
        return originDate.getFullYear();
    };

    utilService.nextDay = function (originDateStr) {
        return utilService.getDate(1, originDateStr);
    };

    utilService.nextMonth = function (originDateStr) {
        var originDate = new Date();
        if (originDateStr) {
            originDate = new Date(originDateStr);
        }
        originDate.setMonth(originDate.getMonth() + 1);
        return new Date(originDate);
    };

    utilService.nextYear = function (originDateStr) {
        var originDate = new Date();
        if (originDateStr) {
            originDate = new Date(originDateStr);
        }
        originDate.setFullYear(originDate.getFullYear() + 1);
        return new Date(originDate);
    };

    utilService.preDay = function (originDateStr) {
        return utilService.getDate(-1, originDateStr);
    };

    utilService.preMonth = function (originDateStr) {
        var originDate = new Date();
        if (originDateStr) {
            originDate = new Date(originDateStr);
        }
        originDate.setMonth(originDate.getMonth() - 1);
        return new Date(originDate);
    };

    utilService.preYear = function (originDateStr) {
        var originDate = new Date();
        if (originDateStr) {
            originDate = new Date(originDateStr);
        }
        originDate.setFullYear(originDate.getFullYear() - 1);
        return new Date(originDate);
    };
    return utilService;
}]);