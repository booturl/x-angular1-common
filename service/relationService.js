/**
 * @Description 自定义tab，实现在不同功能模块页面之间的tab切换，数据可以保留
 * @author Administrator
 * @date 2016-01-15
 * @type {module|*}
 * @version V1.0
 */
if (!relationModule)
    var relationModule = angular.module("app.service.relations", []);

relationModule.factory('relationService',
    function () {
        var relationService = {};
        relationService.relations = {};
        /**
         * 将新的relations加入到relationService.relations
         **/
        relationService.appendRelation = function (relations) {
            relationService.relations = relationService.relations
            || {};
            relations = relations || {};
            // 老数据：判断key是否存在
            for (var key_rel in relations) {
                var hasKey = false;
                for (var key_ser in relationService.relations) {
                    if (angular.equals(key_rel, key_ser)) {
                        hasKey = true;
                        break;
                    }
                }
                if (!hasKey)// 新数据：插入
                {
                    if (relations[key_rel]
                        && relationService.relations) {
                        relationService.relations[key_rel] = relations[key_rel];
                    }
                } else {
                    // 老数据：判断key里某个对象是否存在
                    relationService.relations[key_rel] = relationService.relations[key_rel]
                    || {};
                    relations[key_rel] = relations[key_rel]
                    || {};
                    for (var name_rel in relations[key_rel]) {
                        var hasName = false;
                        for (var name_ser in relationService.relations[key_rel]) {
                            if (angular.equals(name_rel,
                                    name_ser)) {
                                hasName = true;
                                break;
                            }
                        }
                        if (!hasName)// 新数据：插入
                        {
                            relationService.relations[key_rel][name_rel] = relations[key_rel][name_rel];
                        } else {
                            // 老数据：判断对象里某个字段是否存在
                            relations[key_rel][name_rel] = relations[key_rel][name_rel]
                            || {};
                            relationService.relations[key_rel][name_rel] = relationService.relations[key_rel][name_rel]
                            || {};
                            for (var field_rel in relations[key_rel][name_rel]) {
                                // 未存在则插入，已存在则覆盖
                                relationService.relations[key_rel][name_rel][field_rel] = relations[key_rel][name_rel][field_rel];
                            }
                        }
                    }
                }
            }
        };

        /**
         * 将一个数组转化成relations的固定格式 list ——
         * 要转化成relations的数组，必须传 key ——
         * 此数组对象在relations中对应的key，必须传 fieldName ——
         * 数组转化成对象时作为key的主字段名，可不传，默认情况下fieldName=key
         * 转化成功之后无需再调用relationService.appendRelation(relations)，此方法中已调用
         */
        relationService.listTransToRelations = function (list, key, fieldName) {
            list = list || [];
            key = key || '';
            if (!fieldName) {
                fieldName = key;
            }
            list = list || [];
            key = key || '';
            var relations = {};
            var valueObj = {};

            for (var i = 0; i < list.length; i++) {
                valueObj[list[i][fieldName]] = list[i];
            }
            relations[key] = valueObj;
            relationService.appendRelation(relations);
        };

        /**
         * 给relation对象重新定义一个key，然后加入relationService.relations
         * relationObj —— relation对象 key —— 更换后的key，必须传
         * fieldName —— 被转换时的主字段名，可不传，默认情况下fieldName=key
         */
        relationService.changeRelationsKey = function (relationObj, key, fieldName) {
            relationObj = relationObj || {};
            key = key || '';
            if (!fieldName) {
                fieldName = key;
            }
            var newRelationObj = {};
            for (var field_old in relationObj) {
                var field_new = relationObj[field_old][fieldName];
                newRelationObj['' + field_new] = relationObj[''
                + field_old];
            }
            var relation = {};
            relation[key] = newRelationObj;
            relationService.appendRelation(relation);
        };

        relationService.clearRelations = function(){
            relationService.relations = {};
        }

        return relationService;
    })
;