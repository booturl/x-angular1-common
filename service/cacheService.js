/**
 * @author hedongyang
 * @date 2016-8-22
 */
if (!cacheModule)
    var cacheModule = angular.module("app.service.cache", []);

/**
 * 缓存数据更新
 */
cacheModule.factory(
    'cacheService', ['$http',
        function ($http) {
            var cacheService = {};

            return cacheService;
        }]);
