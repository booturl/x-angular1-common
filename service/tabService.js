/**
 * @Title tab
 * @Description 自定义tab，实现在不同功能模块页面之间的tab切换，数据可以保留
 * @author Administrator
 * @date 2016-01-15
 * @version V1.0
 */
if (!tabServiceModule)
    var tabServiceModule = angular.module("app.service.tab", []);

tabServiceModule.factory('tabService',
    function () {
        var tabService = {};
        /**
         * 初始化Tab和Controller相应保留数据的对象
         *
         * @param controllerName
         *          Controller名称
         * @param $scope
         *          Controller里的作用域，存放与view绑定的数据模型
         * @param $state
         *          状态页面
         * @param $stateParams
         *          进入状态页面的参数
         * @returns {*}
         */
        tabService.initTab = function (controllerName, $scope, $state,
                                       $stateParams) {
            var stateName = $state.current.name;
            var tabName = stateName.split('.')[0];// 获得父状态名称
            if ($scope.$root.tabSelfState)// tab是使用父状态还是子状态页面
                tabName = stateName;
            var url = $scope.$root.$location.url();
            var tab = $scope.$root.tabs[tabName];
            var tabCtl = tab ? tab[controllerName] : undefined;
            var isNewState = true;
            var isNewController = true;
            if (tab && tabCtl && tab.stateUrl == url) {// 重新进入页面，获取所有保留的数据
                isNewState = false;
                isNewController = false;
                for (var key in tabCtl) {
                    $scope[key] = tabCtl[key];
                }
            }
            else if (tab && tabCtl && tab.stateUrl !== url) {// 同一父状态页面子状态页面之间的切换，新的子页面
                for (var key in tab) {// 全新的子页面，所以删除老页面所有的属性和control
                    delete tab[key];
                }
                // isNewController = false;
                // for ( var key in tabCtl)
                // {
                // if (key !== 'keepParamNames' &&
                // tabCtl.keepParamNames.indexOf
                // (key) == -1)
                // {// 获取整个父状态保留的数据
                // delete tabCtl[key];
                // continue;
                // }
                // $scope[key] = tabCtl[key];
                // }
            }
            else if (tab && tab.stateUrl == url)// 同一页面加载不同的Controller
                isNewState = false;
            else if (!tab) {// 新页面
                tab = $scope.$root.tabs[tabName] = {};
                $scope.$root.tabNames = $scope.$root.tabNames || [];
                $scope.$root.tabNames.push(tabName);
            }
            if (isNewState) {// 新页面tab信息赋值
                tab.stateName = stateName;
                tab.stateParams = angular.copy($stateParams);
                tab.stateUrl = url;
                tab.stateTitle = $state.current.data.title;
            }
            if (isNewController) {// 新Controller
                tabCtl = tab[controllerName] = {};
                tabCtl.path = controllerName;
                //tabCtl.keepParamNames = [];
            }
            tabCtl.isNewState = isNewState;
            tabCtl.isNewController = isNewController;
            return tabCtl;
        };
        /**
         * 初始化$scope需要保留的数据
         *
         * @param $scope
         *          对应的作用域
         * @param tabCtl
         *          tab保留数据的对象
         * @param paramName
         *          参数名
         * @param paramValue
         *          初始化参数值
         * @param isKeep
         *          是否整个父状态保留
         */
        tabService.initParam = function ($scope, tabCtl, paramName,
                                         paramValue, isKeep) {
            tabCtl.allSync = tabCtl.allSync || false;
            if (isKeep) {
                var keepTab = $scope.$root.tabKeep = $scope.$root.tabKeep || {};
                var keepCtl = keepTab[tabCtl.path] = keepTab[tabCtl.path] || {};
                $scope[paramName] = tabCtl[paramName] = keepCtl[paramName] = keepCtl[paramName] || tabCtl[paramName] || paramValue;
            }
            else
                $scope[paramName] = tabCtl[paramName] = tabCtl[paramName] || paramValue;

            //if (isKeep && tabCtl.keepParamNames.indexOf(paramName) == -1)
            //    tabCtl.keepParamNames[tabCtl.keepParamNames.length] = paramName;
        };
        /**
         * 监听$scope里的数据，从而保持一致，监听object会引起严重的性能问题
         *
         * @param $scope
         *          对应的作用域
         * @param tabCtl
         *          tab保留数据的对象
         * @param key
         *          参数名
         */
        tabService.watchParam = function ($scope, tabCtl, key) {
            var objectEquality = true;
            if (angular.isDate(tabCtl[key]) || angular.isString(tabCtl[key]) ||
                angular.isNumber(tabCtl[key]) ||
                angular.isUndefined(tabCtl[key])) {
                objectEquality = true;
            } else if (angular.isObject(tabCtl[key]) ||
                angular.isArray(tabCtl[key])) {
                objectEquality = false;
            }

            var keepTab = $scope.$root.tabKeep = $scope.$root.tabKeep || {};
            var keepCtl = keepTab[tabCtl.path];
            $scope.$watch(key, function (newVal, oldVal) {
                if (!tabCtl.allSync) {
                    for (var key2 in tabCtl) {
                        tabCtl[key2] = $scope[key2];
                        if (keepCtl && keepCtl[key2])
                            keepCtl[key2] = tabCtl[key2];
                    }
                    tabCtl.allSync = true;
                }

                if (newVal !== oldVal && tabCtl[key] !== $scope[key]) {
                    tabCtl[key] = $scope[key];
                    if (keepCtl && keepCtl[key])
                        keepCtl[key] = tabCtl[key];
                }
            }, objectEquality);
        };

        /**
         * 清除rootscope缓存的tab信息
         */
        tabService.clearAllTab = function ($rootscope) {
            $rootscope.tabs = {};
            $rootscope.tabNames = [];
        };
        return tabService;
    });