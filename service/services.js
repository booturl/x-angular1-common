/**
 * 确认对话框
 */
serviceModule.factory('confirmService',
    [
        '$http',
        '$uibModal',
        function ($http, $uibModal) {
            var confirmService = {};
            confirmService.result = false;
            confirmService.confirm = function (processOk, msg, title, processCancel,
                                               size, hasCancelBtn) {
                if (!msg) {
                    msg = "是否确定要进行此项操作？";
                }
                if (!size) {
                    size = "lg";
                }
                if (hasCancelBtn == null || hasCancelBtn == undefined) {
                    hasCancelBtn = true;
                }
                if (hasCancelBtn) {
                    if (!title) {
                        title = "确认对话框";
                    }
                }
                else {
                    if (!title) {
                        title = "提示";
                    }
                }

                var modalInstance = $uibModal.open(
                    {
                        templateUrl: 'module/common/service/modal/confirmModal.html',
                        controller: 'confirmModalCtrl',
                        size: size,
                        resolve: {
                            title: function () {
                                return title;
                            },
                            msg: function () {
                                return msg;
                            },
                            hasCancelBtn: function () {
                                return hasCancelBtn;// 隐藏取消按钮，只有一个确定按钮
                            }
                        }
                    });
                modalInstance.result.then(function () {
                    if (processOk) {
                        processOk();
                    }
                    confirmService.result = true;
                    return confirmService.result;
                }, function () {
                    if (processCancel) {
                        processCancel();
                    }
                    confirmService.result = false;
                    return confirmService.result;
                });
            };
            return confirmService;
        }
    ]);

/**
 * 下载
 */
serviceModule.factory('downloadService',
    [
        '$http',
        '$uibModal',
        function ($http, $uibModal) {
            var downloadService = {};
            downloadService.result = false;
            downloadService.confirm = function (processOk, url, title, processCancel, size) {

                if (!size) {
                    size = "lg";
                }
                if (!title) {
                    title = "请点击下载";
                }

                var modalInstance = $uibModal.open(
                    {
                        templateUrl: 'module/common/service/modal/downloadModal.html',
                        controller: 'downloadModalCtrl',
                        size: size,
                        resolve: {
                            title: function () {
                                return title;
                            },
                            url: function () {
                                return url;
                            }
                        }
                    });
                modalInstance.result.then(function () {
                    if (processOk) {
                        processOk();
                    }
                    downloadService.result = true;
                    return downloadService.result;
                }, function () {
                    if (processCancel) {
                        processCancel();
                    }
                    downloadService.result = false;
                    return downloadService.result;
                });
            };
            return downloadService;
        }
    ]);


/**
 * typeahead控件获取数据的方法(联想搜索得到name)
 */
serviceModule.factory(
    'associaSearchService', ['$http', 'alertService',
        function ($http, alertService) {
            var associaSearchService = {};
            associaSearchService.associaSearch = function (scope, input, url, data, process,modelName) {
                if (input) {
                    var urlCommonList = "";
                    if (!url) {
                        urlCommonList = _serverUrl + modelName + "/" + _listUrlSuffix;
                        url = urlCommonList;
                    }
                    if (!data) {
                        data = {
                            searchText: input
                        };
                    }
                    return $http.post(url, data).error(
                        function (response) {
                            if (scope) {
                                alertService.closeAllAlerts(scope);
                                alertService.addAlert(scope, "网络异常，错误信息："
                                + response);
                            }
                        })
                        .then(function (response) {
                            alertService.closeAllAlerts(scope);
                            if (response.data.status == 1) {
                                if (process) {
                                    process(response.data.data.list, response.data.relations);
                                }
                                if (!angular.equals(url, urlCommonList)) {
                                    return response.data.data;
                                } else {
                                    return response.data.data.list;
                                }
                            } else {
                                var info = response.data.info;
                                if (info) {
                                    if (scope) {
                                        alertService.addAlert(scope, info);
                                    }
                                } else {
                                    if (scope) {
                                        alertService.addAlert(scope, "系统异常，结果码为："
                                        + response.data.status);
                                    }
                                }
                            }
                        });
                }
            };
            return associaSearchService;
        }]);
/**
 * 与后台交互
 */
//serviceModule.factory('callService', ['$http', 'alertService', '$rootScope', '$q','$base64','md5',
//    function ($http, alertService, $rootScope, $q,$base64,md5) {
//
//            var callService = {};
//            callService.call = function ($scope, process, url, data,
//                                         method, errorProcess) {
//                try{
//                    if(!method){
//                        method = "POST";
//                    }
//                    //以同步操作的流程形式来操作异步事件
//                    var deferred = $q.defer();
//                    var promise = deferred.promise;
//                    promise.success = function (successFunc) {
//                        promise.then(function (response) {
//                            successFunc(response);
//                        });
//                        return promise;
//                    };
//                    promise.error = function (errorFunc) {
//                        promise.then(null, errorFunc);
//                        return promise;
//                    };
//                    promise.continue = function (successFunc, errorFunc, notifyFunc) {
//                        promise.then(successFunc, errorFunc, notifyFunc);
//                        return promise;
//                    };
//
//                    if (typeof process === 'function') {
//                        promise.success(process);
//                    }
//                    if (typeof errorProcess === 'function') {
//                        promise.error(errorProcess);
//                    }
//                    layer.close();
//                    var layerLoad = layer.load(1);
//                    var success = function (response, status, headers, config) {
//                        layer.close(layerLoad);
//                        if (response.code == 200) {
//                            deferred.resolve(response);
//                        } else {
//                            if(response.code){
//                                layer.msg(_globalJson.msg[response.code]);
//                            }else {
//                                layer.msg(_globalJson.msg["0"]);
//                            }
//                            deferred.reject(response);
//                        }
//                    };
//                    var error = function (response, status, headers, config) {
//                        layer.close(layerLoad);
//                        if(status == -1){
//                            layer.msg("请求失败，系统异常。");
//                        }else{
//                            layer.msg("请求失败，HTTP错误码："+status+"。");
//                        }
//                        deferred.reject(response);
//                    };
//                    var timestamp = new Date().getTime();
//                    var platform = _globalJson.config.platform;
//                    var version = _globalJson.config.version;
//                    var base64Key = _globalJson.config.base64Key;
//                    var clientKey = md5.createHash($base64.encode(base64Key+version+timestamp+platform));
//                    var timeout = _globalJson.config.timeout;
//                    var tokenId = sessionStorage.getItem('tokenId');
//                    var taxitoken = "";
//                    if(tokenId){
//                        taxitoken = tokenId;
//                    }
//                    url = _serverUrl + url;
//                    $http(
//                        {
//                            method: method,
//                            url: url,
//                            timeout: timeout,
//                            headers: {
//                                'platform': platform,
//                                'version':  version,
//                                'timestamp': timestamp,
//                                'clientKey': clientKey,
//                                'taxitoken': taxitoken
//                            },
//                            data: data
//                        }).success(success).error(error);
//                    return promise;
//                }catch (e){
//                    layer.closeAll();
//                    layer.msg("请求失败，系统异常。");
//                    console.error(e);
//                }
//            };
//            return callService;
//
//    }]);
/**
 * 显示错误提示
 */
serviceModule.factory('alertService', ['$http', function ($http) {
    var alertService = {};
    alertService.addAlert = function ($scope, msg, type) {
        if (!type)
            type = "danger";
        if (!$scope.alerts)
            $scope.alerts = [];

        var alert = {
            type: type,
            msg: msg
        };

        // success is always at the first and there is only one success alert
        if (type == "success") {
            $scope.alerts = []; // remove all
            $scope.alerts.unshift(alert);
        } else {
            if ($scope.alerts.length > 0 && $scope.alerts[0].type == "success")
                $scope.alerts.shift();

            $scope.alerts.push(alert);
        }

        if (!alertService.scope) {// 将关闭alert方法注入到scope里
            alertService.scope = $scope;
            alertService.scope.closeAlert = function (index) {
                alertService.closeOneAlert(alertService.scope, index);
            };
            alertService.scope.closeAllAlerts = function () {
                alertService.closeAllAlerts(alertService.scope);
            };
        }

    };
    alertService.closeOneAlert = function ($scope, index) {
        if ($scope && $scope.alerts) {
            if (index) {
                $scope.alerts.splice(index, 1);
            } else {
                $scope.alerts.splice(0, 1);
            }
        }
    };
    alertService.closeAllAlerts = function ($scope) {
        if ($scope && $scope.alerts) {
            $scope.alerts.splice(0);
        }
    };
    return alertService;
}]);


// multiSelect不传的话默认是单选（false）,size不传的话默认是"lg"(大窗口)
serviceModule.factory('modalService',
    [
        '$uibModal',
        function ($uibModal) {
            var modalService = {};
            modalService.openModal = function (templateUrl, controller, selectedData,
                                               modalProcessOK, multiSelect, filter, size, modalProcessCancel,
                                               extraParam) {
                var size1 = size || '';
                if (!size1) {
                    size = "lg";
                }
                if (!multiSelect) {
                    multiSelect = false;
                }
                //var oriSelectedData = [];
                //if (typeof oriSelectedData == 'undefined')
                oriSelectedData = angular.copy(selectedData);
                //else
                //    angular.copy(selectedData, oriSelectedData);
                var modalInstance = $uibModal.open(
                    {
                        templateUrl: templateUrl,
                        controller: controller,
                        size: size,
                        animation: true,
                        resolve: {
                            selectedData: function () {
                                return selectedData;
                            },
                            multiSelect: function () {
                                return multiSelect;
                            },
                            filter: function () {
                                return filter;
                            },
                            extraParam: function () {// 除了multiSelect、
                                // filter等参数之外要传递的参数
                                return extraParam;
                            }
                        }
                    });
                modalInstance.openSuccess = function (func) {
                    modalInstance.opened.then(func);
                    return modalInstance;
                };
                modalInstance.openFail = function (func) {
                    modalInstance.opened.then(null, func);
                    return modalInstance;
                };
                modalInstance.success = function (func) {
                    modalInstance.result.then(func);
                    return modalInstance;
                };
                modalInstance.error = function (func) {
                    modalInstance.result.then(null, function (reason) {
                        angular.copy(oriSelectedData, selectedData);
                        func(reason);
                    });
                    return modalInstance;
                };
                modalInstance.continue = function (successFunc, errorFunc, notifyFunc) {
                    modalInstance.result.then(successFunc, errorFunc, notifyFunc);
                    return modalInstance;
                };

                if (modalProcessOK)
                    modalInstance.success(modalProcessOK);
                if (!modalProcessCancel) {
                    modalProcessCancel = function () {
                        console.log('Modal dismissed at: ' + new Date());
                    };
                }
                if (modalProcessCancel)
                    modalInstance.error(modalProcessCancel);

                //modalInstance.result.then(function (data) {
                //    modalProcessOK(data);
                //}, function () {
                //    angular.copy(oriSelectedData, selectedData);
                //    if (!modalProcessCancel) {
                //        modalProcessCancel = function () {
                //            console.log('Modal dismissed at: ' + new Date());
                //        };
                //    }
                //    modalProcessCancel();
                //});
                return modalInstance;
            };
            return modalService;
        }
    ]);


// ----------------------------------------------------------------------
// 即时库存Modal封装 End BY GAOSHI
// ----------------------------------------------------------------------

/**
 * 日期时间处理 用于辅助datepicker控件
 */
serviceModule.factory('dateService',
    [
        function () {
            var dateService = {};

            var defaultMinDate = -2209017600000; // 1900-01-01
            var defaultMaxDate = 4102416000000; // 2100-01-01

            dateService.setDefaultMinAndMax = function (minDate, maxDate) {
                defaultMinDate = minDate;
                defaultMaxDate = maxDate;
            };

            dateService.getDefaultMinDate = function () {
                return defaultMinDate;
            };

            dateService.getDefaultMaxDate = function () {
                return defaultMaxDate;
            };

            // for independant value
            dateService.updateDate = function (date, minDate, maxDate) {
                if (!date)
                    return date;

                date = date < minDate ? minDate : date;
                date = date > maxDate ? maxDate : date;
                return date;
            };

            dateService.updateMinDate = function (date, minDate, maxDate) {
                minDate = date || defaultMinDate;
                minDate = minDate > maxDate ? maxDate : minDate;
                return minDate;
            };

            dateService.updateMaxDate = function (date, minDate, maxDate) {
                maxDate = date || defaultMaxDate;
                maxDate = maxDate < minDate ? minDate : maxDate;
                return maxDate;
            };

            // for object
            dateService.initDefaultMinAndMax = function (date) {
                date.minDate = defaultMinDate;
                date.maxData = defaultMaxDate;
            };

            dateService.bindBeginAndEndDate = function ($scope, watch_begin, watch_end,
                                                        begin, end) {
                $scope.$watch(watch_begin, function (newVal, oldVal) {
                    if (newVal !== oldVal) {
                        begin.date = dateService.updateDate(begin.date, begin.minDate,
                            begin.maxData);

                        end.minDate = dateService.updateMinDate(begin.date, end.minDate,
                            end.maxData);
                    }
                }, true);

                $scope.$watch(watch_end,
                    function (newVal, oldVal) {
                        if (newVal !== oldVal) {
                            end.date = dateService.updateDate(end.date, end.minDate,
                                end.maxData);
                            begin.maxDate = dateService.updateMaxDate(end.date,
                                begin.minDate, begin.maxData);
                        }
                    }, true);
            };
            return dateService;
        }
    ]);