/**
 * Modify by ervin on 2016-08-17.
 * customer: zhanghaitao
 * 定义服务: 返回模块所有请求http --  后台交互
 *
 * decode --解码   encode--转码
 * md5.createHash  --生成32位字符串
 *
 *
 *  callService.call( null, null, url, data, get, null ).success(function (result) {
        layer.msg( "删除成功");
    }).error(function (result) {

    });
 */
if (!requestModule)
    var requestModule = angular.module("app.service.request", []);
requestModule.factory('callService', ['$http', 'alertService', '$rootScope', '$q','$base64','md5',
    function ($http, alertService, $rootScope,$q,$base64,md5) {
        var callService = {};
        callService.call = function ($scope, process, url, data, method, errorProcess) {
            try{
                //以同步操作的流程形式来操作异步事件
                var deferred = $q.defer();
                var promise = deferred.promise; //调用process和errorProcess
                promise.success = function (successFunc) {
                    promise.then(function (response) {
                        successFunc(response);
                    });
                    return promise;
                };
                promise.error = function (errorFunc) {
                    promise.then(null, errorFunc);
                    return promise;
                };
                promise.continue = function (successFunc, errorFunc, notifyFunc) {
                    promise.then(successFunc, errorFunc, notifyFunc);
                    return promise;
                };

                if (typeof process === 'function') {
                    promise.success(process);
                }
                if (typeof errorProcess === 'function') {
                    promise.error(errorProcess);
                }
               // layer.closeAll();
                //var layerLoad = layer.load(1);
                var success = function (response, status, headers, config) {
                    //layer.close(layerLoad);
                    if (response.code == _globalJson.config.codeOK) {
                        //Promise对象的状态变为“成功”，触发。success()
                        deferred.resolve(response);
                    } else {
                        if(response.code){
                            layer.msg(_globalJson.msg[response.code]);
                        }else {
                            layer.msg(_globalJson.msg["0"]);
                        }
                        deferred.reject(response);//Promise对象的状态变为“失败”，触发。error()
                    }
                };
                var error = function (response, status, headers, config) {
                   // layer.close(layerLoad);
                    if(status == -1){
                        layer.msg("请求失败，系统异常!!!");
                    }else{
                        layer.msg("请求失败，HTTP错误码："+status+"。");
                    }
                    deferred.reject(response);
                };
                switch (_globalJson.config.projectName){
                    case "taxi":
                        var timestamp = new Date().getTime();
                        var platform = _globalJson.config.platform;
                        var version = _globalJson.config.version;
                        var base64Key = _globalJson.config.base64Key;
                        var clientKey = md5.createHash($base64.encode(base64Key+version+timestamp+platform));
                        var timeout = _globalJson.config.timeout;
                        var tokenId = sessionStorage.getItem('tokenId');
                        var taxitoken = "";
                        if(tokenId){
                            taxitoken = tokenId;
                        }
                        url = _serverUrl + url;
                        $http(
                            {
                                method: method,
                                url: url,
                                timeout: timeout,
                                headers: {
                                    'platform': platform,
                                    'version':  version,
                                    'timestamp': timestamp,
                                    'clientKey': clientKey,
                                    'taxitoken': taxitoken
                                },
                                data: data
                            }).success(success).error(error);
                        break;
                    case "bus":
                        var timeout = _globalJson.config.timeout;  //超时时间
                        var jsonData = {
                            'data': data, //{"version": version, "data": angular.toJson(data)}
                            'version': _globalJson.config.version
                        };

                        if( url != loginURL_first ){
                            url = sessionStorage.getItem('url') + "/" + url + "?token=" + sessionStorage.getItem('token');
                        }
                        $http({
                            method: method,
                            url: url,
                            timeout: timeout,
                            data: jsonData
                        }).success(success).error(error);
                        break;
                }
                return promise;
            }catch (e){
                //layer.closeAll();
                layer.msg("请求失败，系统异常。");
                console.error(e);
            }
        };
        return callService;
    }]);


/**
 * @Title  http请求
 * @author zhanghaitao
 * @Description
 * 定义服务: 返回模块所有请求http
 * 实现业务:
 *          1.界面http请求; 2.下拉取值
 * decode --解码   encode--转码
 * md5.createHash  --生成32位字符串
 * @date  2016-07-19
 * @version V1.0
 */
requestModule.factory('requestService',function ( $http, $q, $base64, md5, $rootScope ) {
    return {
        call: function (method, url, data) {
            // 声明延后执行，表示要去监控后面的执行
            var deferred = $q.defer();
            /*if(!headers){
                headers = getHeaders();
            }*/
            if( url != loginURL_first ){
                url = sessionStorage.getItem('url') + "/" + url + "?token=" + sessionStorage.getItem('token');
            }

            var jsonData = {
                'data': data,
                'version': $rootScope.selectCode("config").version
            };

            var timeout = constant.timeout;

            $http({
                method: method,
                url: url,
                //headers: headers,
                timeout: timeout,
                data: jsonData
            }).success(function (data, status, headers, config) {
                // 声明执行成功，即http请求数据成功，可以返回数据了
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                // 声明执行失败，即服务器返回错误
                deferred.reject(data);
            });
            // 返回承诺，这里并不是最终数据，而是访问最终数据的API
            return deferred.promise;
        },
        selectCall: function ( method, url, headers, data ) {

            if( url != loginURL_first ){
                url = sessionStorage.getItem('url') + "/" + url + "?token=" + sessionStorage.getItem('token');
            }
            $http({
                method: method,
                url: url,
                data: data
            }).success(function ( result ) {
                // 声明执行成功，即http请求数据成功，可以返回数据了
                if( errorCode.codeOK == result.code ){
                    //var jsonData = JSON.parse(result.data);
                    //sessionStorage.setItem('busTypeOption', jsonData.busTypeOption);
                    //sessionStorage.setItem('busTeamOption', jsonData.busTeamOption);
                    sessionStorage.setItem('busOption', result.data);
                }
            }).error(function (  ) {
                // 声明执行失败，即服务器返回错误
                console.log("请求下拉参数值失败!!!")
            });
        }
    }
});

/**
 * 获取下拉框值  车辆种类  车队列表
 */
/*callService.call( null, null, busOption, null, get, null ).success(function (result) {
    $scope.busTypeOption = JSON.parse(result.data).busTypeOption;
    $scope.busTeamOption = JSON.parse(result.data).busTeamOption;
});*/

/**
 * 获取下拉框值  客户列表
 */
/*callService.call( null, null, busCustomerOption, null, get, null ).success(function (result) {
    $scope.customerOption = JSON.parse(result.data).customerOption;
});*/

/*app.factory('dirDate', function () {
    return {
        call: function (dirName, ngModel) {

        }
    }
    console.log(dirName + "  " + ngModel);
    console.dir(template);
    $(".input-group date form_date .form-control").attr({"id": ngModel});
    $("#" + ngModel).attr({"ng-model": ngModel});
});*/


