if (!fileUploaderModule)
    var fileUploaderModule = angular.module("app.service.fileUpload", []);

/**
 * 文件上传service
 *
 * @author zhangqijian
 * @date 2015-04-24
 */
// 文件上传service
fileUploaderModule
    .factory(
    'rymFileUploader',
    [
        'FileUploader',
        '$log',
        'initDataService',
        function (FileUploader, $log, initDataService) {
            // The inheritance. See
            // https://github.com/nervgh/angular-file-upload/blob/v1.0.2/src/module.js#L686

            /**
             * 继承原来的FileUploader
             */
            FileUploader.inherit(rymFileUploader, FileUploader);

            /**
             * 构造方法
             */
            function rymFileUploader(options) {
                rymFileUploader.super_.apply(this, arguments);

                // 添加默认的几个filter 。文件类型，最多几个文件，文件最大值，文件唯一
                this.filters.unshift(
                    {
                        name: 'types',
                        fn: this._typesFilter
                    });
                this.filters.unshift(
                    {
                        name: 'maxQueueLength',
                        fn: this._maxQueueLengthFilter
                    });
                this.filters.unshift(
                    {
                        name: 'maxSize',
                        fn: this._maxSizeFilter
                    });
                this.filters.unshift(
                    {
                        name: 'uniqueQueue',
                        fn: this._uniqueQueueFilter
                    });
                this.filters.unshift(
                    {
                        name: 'uniqueFile',
                        fn: this._uniqueFileFilter
                    });

                rymFileUploader.prototype
                    .setFilterConfig(initDataService.fileUploaderFilters["default"]);
                rymFileUploader.prototype.setServerFiles([]);
            }

            /**
             *
             * 可自定义一个自己的方法在一个新的rymFileUploader 例： 打印这个对象的队列 和 接收名称
             */
            rymFileUploader.prototype.printInfo = function () {
                $log.info(this.queue);
                $log.info("服务器端要接收文件的接收名称:" + this.alias);
            };

            // ---------------------------------------------------------
            // Start Filter @date 2015-04-24
            // ---------------------------------------------------------
            /**
             * 获取filterConfig
             * 创建rymFileUploader时，会使用default的fileUploaderFilters
             */
            rymFileUploader.prototype.getFilterConfig = function () {
                return rymFileUploader.prototype.filterConfig;
            };

            /**
             * 设置filterConfig
             * 创建rymFileUploader时，会使用default的fileUploaderFilters
             */
            rymFileUploader.prototype.setFilterConfig = function (filterConfig) {
                rymFileUploader.prototype.filterConfig = filterConfig || {};
            };

            /**
             * 设置当前服务器已有的文件名 创建rymFileUploader时，会使用default的fileUploaderFilters
             */
            rymFileUploader.prototype.setServerFiles = function (serverFiles) {
                rymFileUploader.prototype.serverFiles = serverFiles || [];
            };

            /**
             * 文件类型过滤器 系统中已经有默认的过滤器： folder 和 queueLimit 说明：
             * 这里可以声明好几个filter，如果在页面中没有配置fitlers 那么他会验证全部的filter，如果有指定filter
             * 那么只验证指定的filter
             */
            rymFileUploader.prototype._typesFilter = function (item, options) {
                var fileContentType = item.name.match(/^(.*)(\.)(.{1,8})$/)[3]; // 这个文件类型正则很有用：）
                var filterConfig = rymFileUploader.prototype.getFilterConfig();
                console.dir("filterConfig:");
                console.dir(filterConfig);
                for (var i = 0; i < filterConfig.fileTypes.length; i++) {
                    if (fileContentType.toLowerCase() === filterConfig.fileTypes[i]
                            .toLowerCase())
                        return true;
                }
                return false;
            };

            /**
             * 最多几个文件
             */
            rymFileUploader.prototype._maxQueueLengthFilter = function (item, options) {
                var filterConfig = rymFileUploader.prototype.getFilterConfig();
                var maxQueue = filterConfig.maxQueue || 10;
                return this.queue.length < maxQueue;
            };

            /**
             * 文件大小
             */
            rymFileUploader.prototype._maxSizeFilter = function (item, options) {
                var filterConfig = rymFileUploader.prototype.getFilterConfig();
                var maxSize = filterConfig.maxSize || (1024 * 1024 * 1);
                return item.size <= maxSize;
            };

            /**
             * Queue里文件唯一
             */
            rymFileUploader.prototype._uniqueQueueFilter = function (item,
                                                                     options) {
                for (var i = 0; i < this.queue.length; i++)
                    if (angular.equals(item, this.queue[i].file))
                        return false;
                return true;
            };

            /**
             * 服务端文件唯一
             */
            rymFileUploader.prototype._uniqueFileFilter = function (item,
                                                                    options) {
                for (var i = 0; i < this.serverFiles.length; i++)
                    if (angular.equals(item, this.serverFiles[i]))
                        return false;
                return true;
            };

            // ---------------------------------------------------------
            // END Filter
            // ---------------------------------------------------------

            // ===========================================================================

            // ---------------------------------------------------------
            // callBack Start | 以下是回调函数，默认已经写好了，如果有需要修改，请自己在实现类重写覆盖该方法。
            // ---------------------------------------------------------
            /**
             * 此处封装目的是为了在服务器成功返回时候，前端js会因为服务器成功返回而标志文件上传成功，
             * 但是具体的有没有成功是靠返回值来判断，而不是成功返回就判断文件上传成功，故 作此封装来统一判定文件是否上传成功，
             * 因要对上传成功后做封装，所以把全体的回掉函数都做一个封装，虽然看起来其他函数没必要，但是还是统一吧，入口都一样。
             *
             * 我们直接设置回调函数，fileLoader.rymCallback.onWhenAddingFileFailed=function(){//your
               * code} 如果觉得代码很长 : var rymCallback = fileLoader.rymCallback ;
             * rymCallback.onWhenAddingFileFailed =function(){//your code}
             */
            rymFileUploader.prototype.rymCallback = {};
            /**
             * 当文件添加失败时
             */
            rymFileUploader.prototype.onWhenAddingFileFailed = function (item,
                                                                         filter, options) {
                switch (filter.name) {
                    case "types":
                        item.errorMsg = "文件类型不支持!";
                        break;
                    case "maxSize":
                        item.errorMsg = "超过最大文件大小!";
                        break;
                    case "uniqueQueue":
                        item.errorMsg = "上传队列已有同名文件!";
                        break;
                    case "uniqueFile":
                        item.errorMsg = "已有同名文件!";
                        break;
                    case "maxQueueLength":
                        item.errorMsg = "超过最大可同时上传文件数！";
                        break;
                    default:
                        item.errorMsg = "未知错误！";
                        break;
                }

                if (angular.isFunction(this.rymCallback.onWhenAddingFileFailed))
                    this.rymCallback
                        .onWhenAddingFileFailed(item, filter, options);
                $log.info('onWhenAddingFileFailed', item, filter, options);
            };
            /**
             * 当文件文件添加成功后
             */
            rymFileUploader.prototype.onAfterAddingFile = function (fileItem) {
                if (angular.isFunction(this.rymCallback.onAfterAddingFile))
                    this.rymCallback.onAfterAddingFile(fileItem);
                $log.info('onAfterAddingFile', fileItem);
            };
            /**
             * 当全部添加成功后
             */
            rymFileUploader.prototype.onAfterAddingAll = function (addedFileItems) {
                if (angular.isFunction(this.rymCallback.onAfterAddingAll))
                    this.rymCallback.onAfterAddingAll(addedFileItems);
                $log.info('onAfterAddingAll', addedFileItems);
            };
            /**
             * <单个> 当要上传之前，
             */
            rymFileUploader.prototype.onBeforeUploadItem = function (item) {
                if (angular.isFunction(this.rymCallback.onBeforeUploadItem))
                    this.rymCallback.onBeforeUploadItem(item);
                $log.info('onBeforeUploadItem', item);
            };
            /**
             * <单个> 上传进行中时
             */
            rymFileUploader.prototype.onProgressItem = function (fileItem,
                                                                 progress) {
                if (angular.isFunction(this.rymCallback.onProgressItem))
                    this.rymCallback.onProgressItem(fileItem, progress);
                $log.info('onProgressItem', fileItem, progress);
            };
            /**
             * 或全部一起上传时
             */
            rymFileUploader.prototype.onProgressAll = function (progress) {
                if (angular.isFunction(this.rymCallback.onProgressAll))
                    this.rymCallback.onProgressAll(progress);
                $log.info('onProgressAll', progress);

            };
            /**
             * 单一个文件成功上传时，可以返回服务器response
             */
            rymFileUploader.prototype.onSuccessItem = function (fileItem,
                                                                response, status, headers) {
                if (angular.isFunction(this.rymCallback.onSuccessItem))
                    this.rymCallback.onSuccessItem(fileItem, response, status,
                        headers);
                $log.info('onSuccessItem', fileItem, response, status, headers);
            };
            /**
             * 单个文件上传失败时，可以返回服务器response
             */
            rymFileUploader.prototype.onErrorItem = function (fileItem,
                                                              response, status, headers) {
                if (angular.isFunction(this.rymCallback.onErrorItem))
                    this.rymCallback.onErrorItem(fileItem, response, status,
                        headers);
                $log.info('onErrorItem', fileItem, response, status, headers);
            };
            /**
             * 当取消上传时，目前已经正在上传中状态，可以返回服务器response
             */
            rymFileUploader.prototype.onCancelItem = function (fileItem,
                                                               response, status, headers) {
                if (angular.isFunction(this.rymCallback.onCancelItem))
                    this.rymCallback.onCancelItem(fileItem, response, status,
                        headers);
                $log.info('onCancelItem', fileItem, response, status, headers);
            };
            /**
             * 当一个文件上传完成时，可以返回服务器response
             */
            rymFileUploader.prototype.onCompleteItem = function (fileItem,
                                                                 response, status, headers) {
                if (angular.isFunction(this.rymCallback.onCompleteItem))
                    this.rymCallback.onCompleteItem(fileItem, response, status,
                        headers);
                $log
                    .info('onCompleteItem', fileItem, response, status, headers);
            };
            /**
             * 当全部完成时
             */
            rymFileUploader.prototype.onCompleteAll = function () {
                if (angular.isFunction(this.rymCallback.onCompleteAll))
                    this.rymCallback.onCompleteAll();
                $log.info('onCompleteAll');
            };

            // ---------------------------------------------------------
            // callBack End
            // ---------------------------------------------------------

            return rymFileUploader;
        }
    ]);
