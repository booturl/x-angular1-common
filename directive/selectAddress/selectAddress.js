/**
 * @Title  selectAddress -地址选择器
 * @Description 地址选择器基本功能
 * @author 'network'
 * @version V1.0
 *
 * @Description 修复网络版事件BUG，新增排除省市区选择功能
 * @author hedongyang
 * @date 2016-07-5 19:35
 *
 * @Description 新增省市名称简写全写/功能
 * @author hedongyang
 * @date 2016-07-6 20:20
 *
 * @Description 新增城市区号获取功能
 * @author hedongyang
 * @date 2016-08-19 20:20
 *
 * @Description 修复特区区号选择BUG
 * @author hedongyang
 * @date 2016-08-23 16:20
 *
 * @Description 使用layer弹层提示
 * @author hedongyang
 * @date 2016-08-24 10:23
 */
if (!selectAddressModule)
    var selectAddressModule = angular.module("app.directive.selectAddress", []);

selectAddressModule.directive('selectAddress', function ($http, $q, $compile) {
    var cityURL, delay, templateURL;
    delay = $q.defer();
    templateURL = 'bower_components/yaotaxi-angular/directive/selectAddress/selectAddress.html';
    cityURL = 'bower_components/yaotaxi-angular/directive/selectAddress/city.min.json';
    var areaCodeURL = 'bower_components/yaotaxi-angular/directive/selectAddress/areaCode.min.json';

    $http.get(cityURL).success(function (cityData) {
        $http.get(areaCodeURL).success(function (areaCodeData) {
            var data = {
                cityData:cityData,
                areaCodeData:areaCodeData
            };
            return delay.resolve(data);
        });
    });
    return {
        restrict: 'A',
        //priority:1,//优先级
        //terminal:true,//是否运行低优先级的
        //replace:true,//是否替换，默认false当作子元素插入
        //scope:true,//继承外部$scope作用域
        //transclude:true,//嵌入，将指令包涵的内容作为子节点嵌入ng-transclude
        scope: {//隔离作用域 ‘@’绑定字符串，‘=’双向绑定变量，‘&’将引用传递给这个方法
            p: '=', //省
            c: '=', //市
            a: '=', //区
            d: '=', //详细地址
            o: '=',  //区号
            omit: '=',//是否简写，默认false添加省市等尾椎
            unSelectType: '@unSelectType',//不予选择的部分，eg:“a"不予选择地区级
            ngModel: '='
        },
        link: function (scope, element, attrs) {

            var popup;
            popup = {
                element: null,
                backdrop: null,
                show: function () {
                    return this.element.addClass('active');
                },
                hide: function () {
                    this.element.removeClass('active');
                    if(scope.unSelectType.indexOf('c') == -1){
                        if(!scope.c){
                            scope.p = null;
                            scope.a = null;
                            scope.d = null;
                            scope.o = null;
                        }
                    }else{
                        if(scope.unSelectType.indexOf('a') == -1){
                            if(!scope.a){
                                scope.p = null;
                                scope.c = null;
                                scope.o = null;
                                scope.d = null;
                            }
                        }else{
                            if(scope.unSelectType.indexOf('d') == -1){
                                if(!scope.d){
                                    scope.p = null;
                                    scope.c = null;
                                    scope.a = null;
                                    scope.o = null;
                                }
                            }
                        }
                    }
                    return false;
                },
                resize: function () {
                    if (!this.element) {
                        return;
                    }
                    this.element.css({
                        top: -this.element.height() - 30,
                        'margin-left': -this.element.width() / 2
                    });
                    return false;
                },
                focus: function () {
                    $('[ng-model="d"]').focus();
                    return false;
                },
                init: function () {
                    element.on('click keydown', function (event) {
                        popup.show();
                        event.stopPropagation();
                        return false;
                    });
                    $(window).on('click', (function (_this) {
                        return function () {
                            return _this.hide();
                        };
                    })(this));
                    this.element.on('click', function (event) {
                        return event.stopPropagation();
                    });
                    return setTimeout((function (_this) {
                        return function () {
                            _this.element.show();
                            return _this.resize();
                        };
                    })(this), 500);
                }
            };
            //初始化
            return delay.promise.then(function (data) {
                $http.get(templateURL).success(function (template) {
                    var $template;
                    $template = $compile(template)(scope);
                    $('body').append($template);
                    popup.element = $($template[2]);
                    scope.provinces = data.cityData;
                    if(scope.unSelectType.indexOf('o') == -1){
                        scope.areaCodeData = data.areaCodeData;
                    }
                    return popup.init();
                });
                //删除尾称
                var toOmit = function (thisVal) {
                    thisVal = thisVal.replace("省", "");
                    thisVal = thisVal.replace("市", "");
                    thisVal = thisVal.replace("地区", "");
                    thisVal = thisVal.replace("自治州", "");
                    thisVal = thisVal.replace("自治区", "");
                    thisVal = thisVal.replace("特别行政区", "");
                    return thisVal;
                }
                scope.selectClass = function (thisVal, itemVal) {
                    if (!thisVal || !itemVal) {
                        return;
                    }
                    if (!scope.omit) {
                        thisVal = toOmit(thisVal);
                    }
                    return thisVal === itemVal;
                }
                //增加尾称
                scope.omitSwitch = function(name,type){
                    //如果是简单称呼则直接返回
                    if (scope.omit){
                        return name;
                    }
                        var ifCase = false;
                        switch (name){
                            case "北京" :case "天津" :case "重庆" :
                                name += "市";
                                ifCase = true;
                                break;
                            case "宁夏" :case "新疆" :case "内蒙古" :case "广西" :case "西藏" :
                                name += "自治区";
                                ifCase = true;
                                break;
                            case "大兴安岭" :case "昌都" :case "山南" :case "日喀则" :case "那曲" :case "阿里" :case "林芝" :case "吐鲁番"
                                :case "哈密" :case "阿克苏" :case "喀什" :case "和田" :case "塔城" :case "阿勒泰":
                                name += "地区";
                                ifCase = true;
                                break;
                            case "昌吉" :case "博尔塔拉" :case "巴音郭楞" :case "克孜勒苏" :case "伊犁" :case "延边" :case "恩施" :
                            case "湘西" :case "阿坝" :case "甘孜" :case "凉山" :case "黔西南" :case "黔东南" :case "黔南" :
                            case "楚雄" :case "红河" :case "文山" :case "西双版纳" :case "大理" :case "德宏" :case "怒江傈" :
                            case "迪庆" :case "临夏" :case "甘南" :case "海北" :case "黄南" :case "果洛" :case "玉树" :case "海西" :
                                name += "自治州";
                                ifCase = true;
                                break;
                            case "海南" :
                                if(type == "c"){
                                    name += "自治州";
                                }
                                ifCase = true;
                                break;
                            case "香港" :case "澳门" :
                                name += "特别行政区";
                                ifCase = true;
                                break;
                            case "国外" :
                                ifCase = true;
                                break;
                            default:
                                //如果没有case执行则执行
                                if(!ifCase){
                                    if(type == "p")
                                        name += "省";
                                    if(type == "c"){
                                        var length = name.length;
                                        if(name[length - 1] != "市"
                                        && name[length - 1] != "县"
                                        && name[length - 1] != "区"){
                                            name += "市";
                                        }
                                    }
                                }
                        }
                    return name;
                }
                scope.aSet = {
                    p: function (p) {

                        if(scope.unSelectType.indexOf('o') == -1){
                            //如果有区号就赋值，没区号则赋空
                            scope.o = scope.areaCodeData[p];
                        }
                        scope.p = scope.omitSwitch(p,"p");
                        if(scope.p == "国外"){
                            if(scope.unSelectType.indexOf('c') == -1){
                                scope.c = "国外";
                            }
                            if(scope.unSelectType.indexOf('a') == -1){
                                scope.a = "国外";
                            }
                            return scope.d = null;
                        }
                        scope.c = null;
                        scope.a = null;

                        return scope.d = null;
                    },
                    c: function (c) {
                        if(scope.unSelectType.indexOf('o') == -1){
                            scope.o = scope.areaCodeData[c] || scope.areaCodeData[toOmit(scope.p)];
                        }
                        scope.c = scope.omitSwitch(c,"c");
                        scope.a = null;
                        return scope.d = null;
                    },
                    a: function (a) {
                        scope.a = scope.omitSwitch(a,"a");
                        scope.d = null;
                        return popup.focus();
                    },
                    d: function (d) {
                        scope.d = d;
                    }
                };
                scope.showMsg = function(msg){
                    scope.msg = msg;
                    window.setTimeout(function(){
                        scope.msg = null;
                    },3000);
                }
                scope.clear = function () {
                    scope.p = null;
                    scope.c = null;
                    scope.a = null;
                    scope.d = null;
                    return scope.o = null;
                };
                scope.submit = function () {
                    if(!scope.p){
                        return popup.hide();
                    }
                    if(scope.unSelectType.indexOf('c') == -1){
                        if(!scope.c){
                            layer.msg("请选择市区！");
                            return;
                        }
                    }
                    if(scope.unSelectType.indexOf('a') == -1){
                        if(!scope.a){
                            layer.msg("请选择区域！");
                            return;
                        }
                    }
                    if(scope.unSelectType.indexOf('d') == -1){
                        if(!scope.d){
                            layer.msg("请输入具体地址！");
                            return;
                        }
                    }
                    return popup.hide();
                };
                scope.$watch('p', function (newV) {
                    var v, _i, _len, _results;
                    if (newV) {
                        _results = [];
                        for (_i = 0, _len = data.cityData.length; _i < _len; _i++) {
                            v = data.cityData[_i];
                            if (v.p === toOmit(newV)) {
                                _results.push(scope.cities = v.c);
                            }
                        }
                        return _results;
                    } else {
                        return scope.cities = [];
                    }
                });
                if (scope.unSelectType.indexOf('c') == -1) {
                    scope.$watch('c', function (newV) {
                        var v, _i, _len, _ref, _results;
                        if (newV) {
                            _ref = scope.cities;
                            _results = [];
                            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                                v = _ref[_i];
                                if (v.n === toOmit(newV)) {
                                    _results.push(scope.dists = v.a);
                                }
                            }
                            return _results;
                        } else {
                            return scope.dists = [];
                        }
                    });
                }
                return scope.$watch(function () {
                    scope.ngModel = '';
                    if (scope.p) {
                        scope.ngModel += scope.p;
                    }
                    if (scope.c) {
                        scope.ngModel += " " + scope.c;
                    }
                    if (scope.a) {
                        scope.ngModel += " " + scope.a;
                    }
                    if (scope.d) {
                        scope.ngModel += " " + scope.d;
                    }
                    return popup.resize();
                });
            });
        }
    };
});

