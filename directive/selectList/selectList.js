if (!selectedItemsModule)
    var selectedItemsModule = angular.module("app.directive.selectedItems", []);
/**
 * @Title 表格选中数据展示模版
 * @Description 表格页基本功能的封装
 * @author hedongyang
 * @date 2015-11-23
 * @version V1.0
 *
 * @Description 新增多属性展示
 * @author hedongyang
 * @date 2016-08-3
 */
selectedItemsModule.directive('selectedItems', ['$parse', function($parse) {
        return {
            replace: true,
            scope: {},
            templateUrl: 'bower_components/yaotaxi-angular/directive/selectList/selectList.html',
            link: function(scope, element, attrs) {
                scope.selected = scope.$parent.$eval(attrs.selected);
                scope.labelArray = attrs.name.split(",");
                scope.removeItem = function(index) {
                    if (angular.isFunction(scope.$parent.$eval(attrs.removed))) {
                        scope.$parent.$eval(attrs.removed)(index);
                    } else {
                        scope.selected.splice(index, 1);
                    }
                };
                var moveItem = function(arr, fromIndex, toIndex) {
                    arr[fromIndex] = arr.splice(toIndex, 1, arr[fromIndex])[0];
                };
                scope.shiftUp = function(index) {
                    moveItem(scope.selected, index, index - 1);
                };

                scope.shiftDown = function(index) {
                    moveItem(scope.selected, index, index + 1);
                };
            }
        };
    }]);

