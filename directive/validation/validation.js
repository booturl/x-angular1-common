/**
 * Form 数据校验模块模块
 */
if (!validationModule)
    var validationModule = angular.module("app.directive.validation", []);

var NAME_REGEXP = /^([a-zA-Z0-9\u4e00-\u9fa5\·]{1,10})$/i;
var PHONE_REGEXP = /^[+]{0,1}(\d){1,3}[ ]?([-]?((\d)|[ ]){1,12})+$/i;
var CHMOBILE_REGEXP = /^1\d{10}$/i;
var PHONE_CHMOBILE_REGEXP = /^((\d{11})|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$)/i;
var IDCARDNUMBER_REGEXP = /^[1-9]([0-9]{16}|[0-9]{13})[xX0-9]$/i;
var COMPANY_REGEXP = /.*/i;
var ADDRESS_REGEXP = /.*/i;
var ZIPCODE_REGEXP = /^[1-9]\d{5}$/i;
var DATE_REGEXP = /^(\d{4})-(\d{2})-(\d{2})$/i;
var TIME_REGEXP = /^(\d{4})-(\d\d)-(\d\d) (\d\d):(\d\d)(?::(\d\d)(\.\d{1,3})?)?$/i;
var PRICE_REGEXP = /^(0|[1-9][0-9]{0,9})(\.[0-9]{1,2})?$/i;
var CARNUMBER_REGEXP = /^[\u4e00-\u9fa5]{1}[a-zA-Z]{1}[a-zA-Z_0-9]{4}[a-zA-Z_0-9_\u4e00-\u9fa5]$/i;
var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
var QQ_REGEXP = /^[1-9][0-9]{4,12}$/i;
var WEIXIN_REGEXP = /^[A-Za-z][A-Za-z0-9_-]{5,19}$/i;
var PASSWORD_REGEXP = /^[A-Za-z0-9*#]{6,20}$/i;
var TIMEDURATION_REGEXP = /.*/i;
var PERCENT_REGEXP = /^(0|[1-9][0-9]{0,3})(\.[0-9]{1,2})?%$/i;
var INTEGER_REGEXP = /^(0|[1-9][0-9]*)$/i;
var NUMBER_REGEXP = /^[0-9]+$/i;
var UPPERCASE_REGEXP = /^[A-Z]+$/i;
var LOWERCASE_REGEXP = /^[a-z]+$/i;
var LETTER_REGEXP = /^[A-Za-z]+$/i;
var CODE_REGEXP = /^[\w-]+$/i;
var STRING_REGEXP = /.*/i;
var STRING_LICENSE=/^[\u4e00-\u9fa5]{1}[A-Z]{1}[A-Z_0-9]{5}$/i;
var CHSTRING_REGEXP = /^[\u4e00-\u9fa5]+$/i;
var ENSTRING_REGEXP = /^[A-Za-z\s_]+$/i;
var CHENSTRING_REGEXP = /^([\u4e00-\u9fa5A-Za-z\s]+)$/i;
var SEARCH_REGEXP = /^.{0,32}$/i;

var FLOAT_REGEXP_STRING = "/^(0|[1-9][0-9]{0,regexp_m})(\\.[0-9]{1,regexp_n})?$/i";


validationModule.directive('rymValidation', ['$compile', '$document', '$templateCache',
           function($compile, $document, $templateCache) {
              return {
                restrict : 'A',
                require : "ngModel",
                scope :
                {
                  isFocus : '=?',
                  hasError : '=?'
                },
                link : function(scope, element, attr, ctrl) {
                  if (!element || !ctrl)
                    return;

                  // 提示信息弹出框
                  var promtHtml = "<div class=\"page-feedback-position\"> \n"
                                  + "   <span class=\"glyphicon glyphicon-remove form-control-feedback light-red\" ng-if=\"isDirty() && hasError() && isFocus\"></span> \n"
                                  + "   <span class=\"glyphicon glyphicon-ok form-control-feedback light-green\" ng-if=\"isDirty() && !hasError()\"></span> \n"
                                  + "   <span class=\"glyphicon glyphicon-exclamation-sign form-control-feedback light-red\" ng-if=\"!isFocus && hasError()\" ></span> \n"
                                  + "</div> \n"
                                  + "<div ng-if=\"isFocus && isDirty() && hasError()\"> \n"
                                  + "	<div class=\"popover popover-warnning bottom fade in show\"> \n"
                                  + "		<div class=\"arrow\"></div> \n"
                                  + "		<div class=\"popover-inner\"> \n"
                                  + "			<p class=\"popover-title ng-binding ng-show text-center\">输入提示</p> \n"
                                  + "			<div class=\"popover-content ng-binding\"> \n"
                                  + "				<ul style=\"padding-left: 15px;\"> \n"
                                  + "					<li ng-if=\"error.required\">必填项</li> \n"
                                  + "					<li ng-if=\"error.minlength\">输入的数据长度太短</li> \n"
                                  + "					<li ng-if=\"error.maxlength\">输入的数据长度超过限制范围</li> \n"
                                  + "					<li ng-if=\"error.name\">输入的姓名格式不正确</li> \n"
                                  + "					<li ng-if=\"error.phone\">输入的电话、传真格式不正确</li> \n"
                                  + "					<li ng-if=\"error.chMobile\">输入的手机号码格式不正确</li> \n"
                                  + "					<li ng-if=\"error.phoneAndChMobile\">输入的联系号码格式不正确</li> \n"
                                  + "					<li ng-if=\"error.idCardNumber\">输入的身份证号码格式不正确</li> \n"
                                  + "					<li ng-if=\"error.company\">输入的名称格式不正确</li> \n"
                                  + "					<li ng-if=\"error.address\">输入的地址格式不正确</li> \n"
                                  + "					<li ng-if=\"error.zipcode\">输入的邮编格式不正确</li> \n"
                                  + "					<li ng-if=\"error.date\">日期格式为：yyyy-mm-dd，如2015-01-01</li> \n"
                                  + "					<li ng-if=\"error.time\">时间格式为：yyyy-mm-dd hh:mm:ss或hh:mm，如2015-01-01 12:00</li> \n"
                                  + "					<li ng-if=\"error.price\">格式为：xxxx.xx，如123.4, 123.45</li> \n"
                                  + "					<li ng-if=\"error.carNumber\">输入的车牌号码格式不正确</li> \n"
                                  + "					<li ng-if=\"error.email\">输入的邮箱格式不正确</li> \n"
                                  + "					<li ng-if=\"error.qq\">输入的QQ号码格式不正确</li> \n"
                                  + "					<li ng-if=\"error.weixin\">输入的微信号码格式不正确</li> \n"
                                  + "					<li ng-if=\"error.password\">密码格式为：字母大小写、数字、*、#的组合，不低于6位</li> \n"
                                  + "					<li ng-if=\"error.timeDuration\">输入的时间段格式不正确</li> \n"
                                  + "					<li ng-if=\"error.precent\">格式为：xxxx.xx%，如123.45%</li> \n"
                                  + "					<li ng-if=\"error.integer\">格式为：正整数</li> \n"
                                  + "					<li ng-if=\"error.float\">格式为：最大{{m}}位整数和{{n}}位小数</li> \n"
                                  + "					<li ng-if=\"error.number\">格式为：数字</li> \n"
                                  + "					<li ng-if=\"error.upperCase\">格式为：英文大写字母</li> \n"
                                  + "					<li ng-if=\"error.lowerCase\">格式为：英文小写字母</li> \n"
                                  + "					<li ng-if=\"error.letter\">格式为：英文大小写</li> \n"
                                  + "					<li ng-if=\"error.code\">格式为：英文大小写、数字、连接符-、和下划线_</li> \n"
                                  + "					<li ng-if=\"error.string\">输入非法字符</li> \n"
                                  + "					<li ng-if=\"error.license\">格式为：首位中文、大写英文、数字加英语</li> \n"
                                  + "					<li ng-if=\"error.chString\">格式为：中文字符</li> \n"
                                  + "					<li ng-if=\"error.enString\">格式为：英文字符、空格及下划线</li> \n"
                                  + "					<li ng-if=\"error.chEnString\">格式为：中英文字符</li> \n"
                                  + "         <li ng-if=\"error.search\">输入的搜索信息格式不正确，最多支持32个字符</li> \n"
                                  + "         <li ng-if=\"error.funcValidator\">{{errorMsg}}</li> \n"
                                  + "				</ul> \n" + "			</div> \n"
                                  + "		</div> \n" + "	</div> \n" + "</div> \n";

                  // 创建用于提示信息的element
                  var promtEl = angular.element(promtHtml);

                  var $promtTmp = $compile(promtEl)(scope);
                  // Prevent jQuery cache memory leak (template is now redundant
                  // after linking)
                  promtEl.remove();

                  element.after($promtTmp);

                  // HTML控件鼠标事件绑定
                  var documentClickBind = function(event) {
                    if (event.target !== element[0]) {
                      scope.isFocus = false;
                    }
                    else {
                      scope.isFocus = true;
                    }
                  };

                  $document.bind('click', documentClickBind);

                  // HTML控件按键事件
                  var keydown = function(evt, noApply) {
                    scope.keydown(evt);
                  };

                  element.bind('keydown', keydown);

                  scope.keydown = function(evt) {
                    scope.isFocus = true;
                    if (evt.which == 9) {
                      // TAB键，失去焦点
                      scope.isFocus = false;
                    }
                  };

                  scope.hasError = function() {
                    var hasError = false;

                    // Update scope.error and hasError
                    scope.error = ctrl.$error;
                    for ( var prop in ctrl.$error) {
                      if (ctrl.$error.required == true && !ctrl.$dirty) {
                        continue;
                      }

                      hasError = true;
                      break;
                    }

                    return hasError;
                  };

                  // 输入框未动过，不显示
                  scope.isDirty = function() {
                    return ctrl.$dirty;
                  };

                  // 根据类型进行校验，将更新input element的$error对象
                  var typeValidator = function(value) {
                    var validator = attr.rymValidation;

                    switch (validator) {
                      case "name":
                        var regexp = NAME_REGEXP;
                        break;
                      case "phone":
                        var regexp = PHONE_REGEXP;
                        break;
                      case "chMobile":
                        var regexp = CHMOBILE_REGEXP;
                        break;
                      case "phoneAndChMobile":
                         var regexp = PHONE_CHMOBILE_REGEXP;
                         break;
                      case "idCardNumber":
                        var regexp = IDCARDNUMBER_REGEXP;
                        break;
                      case "company":
                        var regexp = COMPANY_REGEXP;
                        break;
                      case "address":
                        var regexp = ADDRESS_REGEXP;
                        break;
                      case "zipcode":
                        var regexp = ZIPCODE_REGEXP;
                        break;
                      case "date":
                        var regexp = DATE_REGEXP;
                        break;
                      case "time":
                        var regexp = TIME_REGEXP;
                        break;
                      case "price":
                        var regexp = PRICE_REGEXP;
                        break;
                      case "carNumber":
                        var regexp = CARNUMBER_REGEXP;
                        break;
                      case "email":
                        var regexp = EMAIL_REGEXP;
                        break;
                      case "qq":
                        var regexp = QQ_REGEXP;
                        break;
                      case "weixin":
                        var regexp = WEIXIN_REGEXP;
                        break;
                      case "password":
                        var regexp = PASSWORD_REGEXP;
                        break;
                      case "precent":
                        var regexp = PRECENT_REGEXP;
                        break;
                      case "integer":
                        var regexp = INTEGER_REGEXP;
                        break;
                      // case "float":
                      // 为支持float_m_n,将在后面处理
                      // var regexp = FLOAT_REGEXP;
                      // break;
                      case "number":
                        var regexp = NUMBER_REGEXP;
                        break;
                      case "upperCase":
                        var regexp = UPPERCASE_REGEXP;
                        break;
                      case "lowerCase":
                        var regexp = LOWERCASE_REGEXP;
                        break;
                      case "letter":
                        var regexp = LETTER_REGEXP;
                        break;
                      case "code":
                        var regexp = CODE_REGEXP;
                        break;
                      case "string":
                        var regexp = STRING_REGEXP;
                        break;
                        case "license":
                            var regexp = STRING_LICENSE;
                            break;
                      case "chString":
                        var regexp = CHSTRING_REGEXP;
                        break;
                      case "enString":
                        var regexp = ENSTRING_REGEXP;
                        break;
                      case "chEnString":
                        var regexp = CHENSTRING_REGEXP;
                        break;
                      case "search":
                        var regexp = SEARCH_REGEXP;
                        break;

                      default:
                        var tmpattr = validator.split("_");
                        validator = tmpattr[0];

                        switch (validator) {
                          case "float":
                            // 如果只写float，默认为float_10_6
                            var m = (tmpattr.length > 1) ? tmpattr[1] : 10;
                            var n = (tmpattr.length > 2) ? tmpattr[2] : 6;

                            scope.m = m;
                            scope.n = n;

                            // 注： 正则表达式中的m,n需要再做一下转换，主要是因为正则表达式里的算式不能直接计算的结果使用
                            var regexp_m = m > 1 ? (m - 1) : 0;
                            var regexp_n = n > 0 ? n : 0;

                            var string = FLOAT_REGEXP_STRING;
                            string = string.replace("regexp_m", regexp_m);
                            string = string.replace("regexp_n", regexp_n);

                            var regexp = eval(string);
                            break;

                          default:
                            return value;
                        }
                    }

                    // Put error info into ctrl.$error
                    var validity = ctrl.$isEmpty(value) || regexp.test(value);
                    ctrl.$setValidity(validator, validity);

                    return value;
                  };

                  // 根据函数进行校验，将更新input element的$error对象
                  var funcValidator = function(value) {
                    var func = attr.rymValidation;
                    var tmpattr = func.split("(");
                    func = tmpattr[0];

                    var validity =
                    {
                      hasError : false,
                      errorMsg : ""
                    };

                    // 调用校验函数
                    validity = eval("scope\.\$parent\." + func + "(value)");

                    ctrl.$setValidity("funcValidator", !validity.hasError);
                    scope.errorMsg = validity.errorMsg;

                    return value;
                  };

                  // 自定义校验函数入口
                  var customValidator = function(value) {
                    if (!attr.rymValidation)
                      return value;

                    if (attr.rymValidation.indexOf("\(\)") > 0) {
                        return funcValidator(value);
                    }else{
                      return typeValidator(value);
                    }
                  };

                  ctrl.$parsers.push(customValidator);
                  ctrl.$formatters.push(customValidator);

                  // destory处理
                  scope.$on('$destroy', function() {
                    $promtTmp.remove();
                    element.unbind('keydown', keydown);
                    $document.unbind('click', documentClickBind);
                  });
                }
              };
            }
        ]);
