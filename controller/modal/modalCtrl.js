if (!modalModule)
    var modalModule = angular.module("app.service.modal", []);
/**
 * 确认对话框（OK / cancel ）
 */
modalModule.controller('confirmModalCtrl', function($scope, $uibModalInstance,
    title, msg, hasCancelBtn) {
  $scope.title = title;
  $scope.msg = msg;
  $scope.if_btn_cancel = hasCancelBtn;
  $scope.ok = function() {
    $uibModalInstance.close();
  };
  $scope.cancel = function() {
    $uibModalInstance.dismiss();
  };
});
/**
 * 文件下载对话框
 */
modalModule.controller('downloadModalCtrl', function($scope, $uibModalInstance,
                                                    title, url) {
    $scope.title = title;
    $scope.url = url;
    $scope.ok = function() {
        $uibModalInstance.close();
    };
    $scope.cancel = function() {
        $uibModalInstance.dismiss();
    };
});


/**
 * 选择时间对话框（OK / cancel ）
 */
modalModule.controller('chooseTimeCtrl', function ($scope, $uibModalInstance,
                                                   multiSelect, selectedData, filter,extraParam) {

    $scope.title = multiSelect;
    $scope.ok = function () {
        $uibModalInstance.close($scope.time);
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});
