if (!gridModule)
    var gridModule = angular.module("app.controller.grid", []);
/**
 * @Title 列表公共控制器
 * @Description 表格页基本功能的封装
 * @author all
 * @date 2015-11-23
 * @version V1.0
 *
 * @Description 新增前置处理器中断能力
 * @author hedongyang
 * @date 2016-07-24
 *
 * @Description 新增layer弹窗
 * @author hedongyang
 * @date 2016-08-14
 *
 * @Description 修复多表格页多选BUG
 * @author hedongyang
 * @date 2016-08-15
 */
var defaultGridOptions = _globalJson.config.defaultGridOptions;
gridModule.value('gridControllerOptions',_globalJson.config.gridModuleOptions)
    .factory(
        'gridController',
        [
            'gridControllerOptions',
            'callService',
            'tabService',
            'initDataService',
            'utilService',
            function (gridControllerOptions, callService, tabService,
                      initDataService, utilService) {
                /**
                 * 初始化列表模块
                 * @param scope
                 * @param options
                 * @param queryAfterInit
                 * @param filter
                 */
                function gridController(scope, options, queryAfterInit, filter) {
                    var settings = angular.copy(gridControllerOptions);
                    utilService.extendDeep(this, settings, options);
                    this.gridOptions = utilService.extendDeep({}, defaultGridOptions, this.gridOptions);

                    this.originFilterParam = angular.copy(this.filterParam);

                    this.attach(scope);
                    var self = this;
                    // 刷新Grid，显示数据
                    if (arguments[2] == undefined)
                        queryAfterInit = true;
                    if (queryAfterInit) {
                        setTimeout(function () {
                            self.query();
                        }, 10);
                    }
                }

                /*****************************************************************
                 * PUBLIC
                 * 取回数据给scope
                 ****************************************************************/
                gridController.prototype.attach = function (scope) {
                    //scope.gridOptions = this.gridOptions;
                    scope[this.gridOptions.data] = this.gridData;
                    scope[this.gridOptions.sumData] = this.sumData;
                    scope[this.gridOptions.totalServerItems] = this.totalServerItems;
                    //scope.gridOptions.selectedItems = this.gridOptions.selectedItems;
                    scope.pagingOptions = this.gridOptions.pagingOptions;
                    scope.filterOptions = this.gridOptions.filterOptions;
                    scope.originFilterParam = this.originFilterParam;
                    this.scope = scope;
                    var self = this;
                    //如果需要控制列的隐藏和显示，则传入columns，不传入gridOptions.columnDefs
                    self.gridOptions.columnDefs = self.gridOptions.columnDefs || [];
                    if ((!self.gridOptions.columnDefs || self.gridOptions.columnDefs.length == 0) && self.columns) {
                        for (fields in self.columns) {
                            if (fields && self.columns[fields]) {
                                self.gridOptions.columnDefs.push(self.columns[fields]);
                            }
                        }
                    }
                    //为grid注入公共方法
                    this.gridOptions.onRegisterApi = function(gridApi) {
                        self.gridApi = gridApi;
                        if (self.gridApi.core) {
                            self.gridApi.core.on.sortChanged(scope, function (grid, sortColumns) {
                                //scope.pagingOptions.sort = sortColumns;
                                self.orderParams = [];
                                for (var i = 0; i < sortColumns.length; i++) {
                                    self.orderParams[i] = {
                                        fieldName: sortColumns[i].field,
                                        order: sortColumns[i].sort.direction
                                    };
                                }
                                self.getPagedDataAsync();
                            });
                        }
                        if (self.gridApi.pagination) {
                            self.gridApi.pagination.on.paginationChanged(scope, function (newPage, pageSize) {
                                self.gridOptions.pagingOptions.currentPage = newPage || 1;
                                self.gridOptions.pagingOptions.pageSize = pageSize;
                                self.getPagedDataAsync();
                            });
                        }

                        //行选择的监听
                        if (self.gridApi.selection) {
                            self.gridApi.selection.on.rowSelectionChanged(scope, function (row) {
                                if (!row.isSelected) {
                                    self.rowUnSelect(row);
                                } else {
                                    self.rowSelect();
                                }
                            });
                            self.gridApi.selection.on.rowSelectionChangedBatch(scope, function (rows) {
                                for (var i = 0; i < rows.length; i++) {
                                    if (!rows[i].isSelected) {
                                        self.rowUnSelect(rows[i]);
                                    }
                                }
                                self.rowSelect();
                            });
                        }
                        self.onAfterRegisterApi(gridApi);
                    };

                    scope.$watch('filterOptions', function (newVal, oldVal) {
                        if (newVal !== oldVal) {
                            self.getPagedDataAsync();
                        }
                    }, true);
                };


                gridController.prototype.setGridData = function (gridData) {
                    this.gridData = gridData;
                    this.scope[this.gridOptions.data] = this.gridData;
                };

                gridController.prototype.getGridData = function () {
                    return this.gridData;
                };

                /**
                 * 关闭弹窗
                 */
                gridController.prototype.cancel = function () {
                    var self = this;

                    self.modalInstance.dismiss();
                };

                /**
                 * 查询按钮的调用方法：
                 */
                gridController.prototype.query = function () {
                    if(this.onBeforeQuery()){
                        return;
                    }
                    if (this.gridOptions.pagingOptions && (!this.gridOptions.pagingOptions.currentPage ||
                        this.gridOptions.pagingOptions.currentPage != 1)) {
                        this.gridApi.pagination.seek(1);
                        // 将currentPage设为1之后会调用到pagingOptions的watch里面的方法，所以不需要再自己调用prototype.getPagedDataAsync()方法，否则会导致调用两次
                    }
                    else {
                        this.getPagedDataAsync();
                    }

                    this.onAfterQuery();
                };
                gridController.prototype.delete = function (entity) {
                    var self = this;
                    if(!entity[self.formDataIdKey]){
                        layer.msg( "找不到要删除的数据");
                        return;
                    }
                    layer.confirm(
                        '确定删除【' + entity[self.formDataNameKey] + '】？', {
                            icon: 3,
                            btn: ['确定','取消'] //按钮
                        }, function(){//确定
                            if(self.onBeforeDelete(entity)){
                                return;
                            }
                            var deleteUrl = self.urlDelete
                                || (self.modelName + "/" + _deleteUrlSuffix + "/" + entity[self.formDataIdKey]);
                            var data = {};
                            data = self.ifJson ? data : angular.toJson(data);
                            callService.call(self.scope, null, deleteUrl, data,"get")
                                .success(function (data) {
                                    layer.msg( "删除成功");
                                    self.getPagedDataAsync();
                                    self.onAfterDelete(data);
                                })
                                .error(function (data) {

                                });
                        }, function(){//取消

                        });
                };
                /**
                 * 重置按钮的调用方法：
                 */
                gridController.prototype.reset = function () {
                    this.onBeforeReset();
                    angular.copy(this.originFilterParam, this.filterParam);
                    this.query();
                    this.onAfterReset();
                };
                /**
                 * 重置按钮的调用方法：
                 */
                gridController.prototype.empty = function () {
                    this.onBeforeEmpty();
                    angular.copy({}, this.filterParam);
                    //this.query();
                    this.onAfterEmpty();
                };
                /**
                 * Callback
                 */
                gridController.prototype.onBeforeQuery = function () {

                };

                /**
                 * Callback
                 */
                gridController.prototype.onAfterQuery = function () {
                };

                /*
                 * 删除前置操作
                 */
                gridController.prototype.onBeforeDelete = function () {

                };
                /*
                 * 删除后置操作
                 */
                gridController.prototype.onAfterDelete = function () {
                };
                /**
                 * Callback
                 */
                gridController.prototype.onBeforeDataQueried = function (data) {

                };
                /**
                 * Callback
                 */
                gridController.prototype.onAfterDataQueried = function (data) {
                };
                /**
                 * Callback
                 */
                gridController.prototype.onBeforeReset = function () {

                };

                /**
                 * Callback
                 */
                gridController.prototype.onAfterReset = function () {

                };
                /**
                 * Callback
                 */
                gridController.prototype.onBeforeEmpty = function () {

                };

                /**
                 * Callback
                 */
                gridController.prototype.onAfterEmpty = function () {

                };

                /**
                 * Callback
                 */
                gridController.prototype.onAfterRegisterApi = function (gridApi) {
                };
                /*****************************************************************
                 * PRIVATE
                 ****************************************************************/
                gridController.prototype.rowUnSelect = function (rowObj) {
                    var self = this;
                    if (rowObj.isSelected) {
                        return;
                    }
                    for (var j = 0; j < self.gridOptions.selectedItems.length; j++) {
                        if (self.gridOptions.selectedItems[j][self.formDataIdKey]
                            == rowObj.entity[self.formDataIdKey]) {
                            self.gridOptions.selectedItems.splice(j, 1);
                            break;
                        }
                    }
                };
                /**
                 * 处理ui-grid中被选中与取消选中
                 * @param row 行对象
                 */
                gridController.prototype.rowSelect = function () {
                    var self = this;
                    //获取当前页的被选中数据，然后对总的selectedItems进行修改
                    var selectedItems = angular.copy(self.gridApi.selection.getSelectedRows())  || [];
                    var index = -1;
                    if (!self.gridOptions.multiSelect) {
                        self.gridOptions.selectedItems[0] = selectedItems[0];
                        return;
                    }
                    //进行复制操作
                    for (var i = 0; i < selectedItems.length; i++) {
                        if(selectedItems[0].$$treeLevel != selectedItems[i].$$treeLevel){//树形结构表格做数据排除
                            continue;
                        }
                        for (var j = 0; j < self.gridOptions.selectedItems.length; j++) {
                            if (self.gridOptions.selectedItems[j][self.formDataIdKey]
                                == selectedItems[i][self.formDataIdKey]) {
                                index = j;
                                break;
                            }
                        }
                        if (index == -1) {
                            self.gridOptions.selectedItems.push(selectedItems[i]);
                        } else {
                            self.gridOptions.selectedItems[j] = selectedItems[i];
                        }
                    }
                };
                /**
                 * 被选中数据，叉叉按钮的onclick事件
                 * @param index
                 */
                gridController.prototype.removeSelectedItems = function (index) {
                    var ctrlSelf = this;
                    var has = false;
                    var item = ctrlSelf.gridOptions.selectedItems[index];
                    if (!item) {
                        return;
                    }
                    for (var i = 0; i < ctrlSelf.gridOptions.data.length; i++) {
                        if(ctrlSelf.gridOptions.data[0].$$treeLevel != ctrlSelf.gridOptions.data[i].$$treeLevel){//树形结构表格做数据排除
                            continue;
                        }
                        if (ctrlSelf.gridOptions.data[i][ctrlSelf.formDataIdKey] == item[ctrlSelf.formDataIdKey]) {
                            ctrlSelf.gridApi.selection.unSelectRow(ctrlSelf.gridOptions.data[i]);
                            has = true;
                            break;
                        }
                    }
                    //找不到说明不在本页里面，直接从selectedItems里面删除
                    if (!has) {
                        ctrlSelf.gridOptions.selectedItems.splice(index, 1);
                    }
                };
                gridController.prototype.removeSelectedItemsAll = function (index) {
                    var ctrlSelf = this;
                    for (var i = 0; i < ctrlSelf.gridOptions.data.length; i++) {
                        if(ctrlSelf.gridOptions.data[0].$$treeLevel != ctrlSelf.gridOptions.data[i].$$treeLevel){//树形结构表格做数据排除
                            continue;
                        }
                        ctrlSelf.gridApi.selection.clearSelectedRows();
                    }
                    ctrlSelf.gridOptions.selectedItems.splice(0, ctrlSelf.gridOptions.selectedItems.length);
                }
                gridController.prototype.setPagingData = function (self, data, page, pageSize) {
                    var self = this;
                    if(!self.onBeforeDataQueried(data)){
                        self.gridData = data;
                        self.gridOptions.data = self.gridData;
                    }
                    //获取选中行
                    setTimeout(function () {
                        for (var i = 0; i < self.gridOptions.selectedItems.length; i++) {
                            for (var k = 0; k < self.gridOptions.data.length; k++) {
                                if (self.gridOptions.selectedItems[i][self.formDataIdKey] == self.gridOptions.data[k][self.formDataIdKey]) {
                                    self.gridApi.selection.selectRow(self.gridOptions.data[k]);
                                    break;
                                }
                            }
                        }
                        if (!self.scope.$$phase) {
                            self.scope.$apply();
                        }
                        self.onAfterDataQueried(data);
                    }, 100)

                };

                gridController.prototype.setSumData = function (self, data, page,
                                                                  pageSize) {
                    self.sumData = data;
                    self.scope[self.gridOptions.sumData] = self.sumData;
                };

                gridController.prototype.getPagedDataAsync = function () {
                    //console.trace();
                    var self = this; // use 'self' to avoid 'this' points to
                    // 'window' in setTimeout
                    if (self.gridOptions.filterOptions.filterText) {

                    }else {
                        var timestamp = new Date().getTime();
                        var listUrl = self.urlList
                            || (self.modelName + "/" + _listUrlSuffix );
                        var data = {};
                        if(self.requestDataFormat == "default"){
                            data =
                            {
                                currentPage : self.gridOptions.pagingOptions.currentPage,
                                pageSize : self.gridOptions.pagingOptions.pageSize,
                                reqBody : self.filterParam
                            };
                        }
                        if(self.requestDataFormat == "busNewFormat"){
                            data =
                            {
                                currentPage : self.gridOptions.pagingOptions.currentPage,
                                pageSize : self.gridOptions.pagingOptions.pageSize,
                                body : self.filterParam
                            };
                        }
                        data = self.ifJson ? data : angular.toJson(data);
                        callService.call(self.scope, null, listUrl, data, "post")
                            .success(function(response){
                                /**********************-start default*********************/
                                if(self.responseDataFormat == "default"){
                                    if(!response.resultInfo){
                                        layer.msg("返回信息格式错误");
                                        return;
                                    }
                                    if(!response.resultInfo.rows){
                                        layer.msg("返回信息格式错误");
                                        return;
                                    }
                                    if(!response.resultInfo.rows.length){
                                        layer.msg("数据库无对应数据");
                                        return;
                                    }
                                    if(self.serverPage){
                                        self.totalServerItems = response.resultInfo.total;
                                        self.gridOptions.totalItems = self.totalServerItems;
                                        self.scope[self.gridOptions.totalServerItems] = self.totalServerItems;
                                        self.setSumData(self, response.resultInfo.rows.length);
                                        self.setPagingData(self, response.resultInfo.rows, response.resultInfo.totalPage,
                                            response.resultInfo.pagesize);
                                    }else{
                                        self.totalServerItems = response.resultInfo.total;
                                        self.gridOptions.totalItems = self.totalServerItems;
                                        self.scope[self.gridOptions.totalServerItems] = self.totalServerItems;
                                        self.setSumData(self, response.resultInfo.rows.length);
                                        self.setPagingData(self, response.resultInfo.rows, response.resultInfo.totalPage,
                                            response.resultInfo.pagesize);
                                    }
                                    //jsonData.slice(firstRow, firstRow + pageSize);
                                }
                                /**********************-end default*********************/
                                /*******************-start busNewFormat*****************/
                                if(self.responseDataFormat == "busNewFormat"){  //是否是新的数据格式
                                    if(!response.data){
                                        layer.msg("返回信息格式错误");
                                        return;
                                    }else{
                                        response.data = angular.fromJson(response.data);
                                    }

                                    if(!response.data.body || !response.data.body.length){
                                        layer.msg("数据库无对应数据");
                                        response.data.body = [];
                                    }
                                    if(self.serverPage){
                                        self.totalServerItems = response.data.total;
                                        self.gridOptions.totalItems = self.totalServerItems;
                                        self.scope[self.gridOptions.totalServerItems] = self.totalServerItems;
                                    }else{
                                        self.totalServerItems = response.data.total;
                                        self.gridOptions.totalItems = self.totalServerItems;
                                        self.scope[self.gridOptions.totalServerItems] = self.totalServerItems;
                                    }
                                    if(self.fixedType == "module"){
                                        self.setSumData(self, self.fixedJson.length);
                                        self.setPagingData(self,  self.fixedJson, response.data.totalPage, response.data.pagesize);
                                    }else{
                                        self.setSumData(self, response.data.body.length);
                                        self.setPagingData(self, response.data.body, response.data.totalPage,
                                            response.data.pagesize);
                                    }
                                }
                                /*******************-end busNewFormat*****************/
                            })
                            .error(function(response){

                            });
                    }
                };


                return gridController;
            }
        ]);
