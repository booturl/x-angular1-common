/**************************************************
 * @Title  弹窗列表
 * @Description 处理业务逻辑
 * @author Administrator
 * @date 2016-08-01 10:22
 * @version V20160810
 *************************************************/
if (!gridModalModule)
    var gridModalModule = angular.module("app.controller.gridModal", []);

/**
 * gridModalModule用于基本的弹窗类列表界面的操作
 */
gridModalModule
    .value('gridModalControllerOptions', _globalJson.config.formModuleOptions)
    .factory('gridModalController', [
        'gridModalControllerOptions', 'callService', 'utilService', 'gridController',
        function (gridModalControllerOptions, callService, utilService, gridController) {
            /**
             * @param {Object}
             * @param [options]
             * @constructor
             */
            function gridModalController(scope, options) {
                utilService.extendDeep(this, gridModalControllerOptions, options);
                this.genFilter(this.extraFilter);
                this.listCtrl = new gridController(scope, this);
                this.attach(scope);
                this.attachListCtrl();
            }

            gridModalController.prototype.genFilter = function (filter) {
                var self = this;
                //1、给控件赋值
                if (filter) {
                    var uiCtrlName = "";
                    for (var i = 0; i < filter.length; i++) {
                        if (!filter[i].fieldName) {
                            continue;
                        }
                        if (!filter[i].value && filter[i].value !== 0) {
                            continue;
                        }
                        if (!self.filterConfig[filter[i].fieldName]) {
                            continue;
                        }

                    }
                }
            };
            /*****************************************************************
             * PUBLIC
             ****************************************************************/
            gridModalController.prototype.attachListCtrl = function () {
                this.listCtrl.onBeforeQuery = this.onBeforeQuery;
                this.listCtrl.onAfterQuery = this.onAfterQuery;
                this.listCtrl.onBeforeDataQueried = this.onBeforeDataQueried;
                this.listCtrl.onAfterDataQueried = this.onAfterDataQueried;
                this.listCtrl.onBeforeReset = this.onBeforeReset;
                this.listCtrl.onAfterReset = this.onAfterReset;
            };

            gridModalController.prototype.attach = function (scope) {
                angular.copy(this.selectedData, scope.selectedData);
                scope.filter = this.filter;
                scope.filterConfig = this.filterConfig;
                scope.uiCtrl = this.uiCtrl;
                scope.filterSrcData = this.filterSrcData;
                scope.alerts = this.alerts;
                scope.filterParam = this.filterParam;
                this.scope = scope;
            };
            /**
             * 关闭弹窗
             */
            gridModalController.prototype.cancel = function () {
                var self = this;
                self.modalInstance.dismiss();
            };
            /**
             * 确定
             */
            gridModalController.prototype.ok = function () {
                var self = this;
                //保存选中选项
                angular.copy(self.listCtrl.gridOptions.selectedItems, self.scope.selectedData);
                self.modalInstance.close(self.listCtrl.gridOptions.selectedItems);
            };
            /**
             * Callback
             */
            gridModalController.prototype.onBeforeQuery = function () {
            };
            /**
             * Callback
             */
            gridModalController.prototype.onAfterQuery = function () {
            };

            /**
             * Callback
             */
            gridModalController.prototype.onBeforeDataQueried = function (data) {
            };
            /**
             * Callback
             */
            gridModalController.prototype.onAfterDataQueried = function (data) {
            };

            /**
             * Callback
             */
            gridModalController.prototype.onBeforeReset = function () {
            };

            /**
             * Callback
             */
            gridModalController.prototype.onAfterReset = function () {
            };

            return gridModalController;
        }
    ]);
