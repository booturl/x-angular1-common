if (!listModule)
    var listModule = angular.module("app.controller.list", []);
/**
 * @Title 列表公共控制器
 * @Description 列表页基本功能的封装
 * @author hedongyang
 * @date 2015-11-23
 * @version V1.0
 */

listModule.value('listControllerOptions',
    _globalJson.config.listModuleOptions)
    .factory(
        'listController',
        [
            'listControllerOptions',
            'callService',
            'tabService',
            'initDataService',
            'utilService',
            function (listControllerOptions, callService, tabService,
                      initDataService, utilService) {
                /**
                 * 初始化列表模块
                 * @param scope
                 * @param options
                 * @param queryAfterInit
                 * @param filter
                 */
                function listController(scope, options, queryAfterInit, filter) {
                    var settings = angular.copy(listControllerOptions);
                    utilService.extendDeep(this, settings, options);

                    this.originFilterParam = angular.copy(this.filterParam);

                    this.attach(scope);
                    var self = this;

                    // 刷新Grid，显示数据
                    if (arguments[2] == undefined)
                        queryAfterInit = true;

                    if (queryAfterInit) {
                        setTimeout(function () {
                            self.query();
                        }, 10);
                    }
                }


                /*****************************************************************
                 * PUBLIC
                 * 取回数据给scope
                 ****************************************************************/
                listController.prototype.attach = function (scope) {
                    
                    scope.originFilterParam = this.originFilterParam;
                    this.scope = scope;
                    var self = this;
                    scope.$watch('filterOptions', function (newVal, oldVal) {
                        if (newVal !== oldVal) {
                            self.getPagedDataAsync();
                        }
                    }, true);
                };


                listController.prototype.setGridData = function (gridData) {
                    this.gridData = gridData;
                    this.scope[this.listOptions.data] = this.gridData;
                };

                listController.prototype.getGridData = function () {
                    return this.gridData;
                };

                /**
                 * 关闭弹窗
                 */
                listController.prototype.cancel = function () {
                    var self = this;

                    self.modalInstance.dismiss();
                };

                /**
                 * 查询按钮的调用方法：
                 */
                listController.prototype.query = function () {
                    if(this.onBeforeQuery()){
                        return;
                    }
                    if (this.listOptions.pagingOptions && (!this.listOptions.pagingOptions.currentPage ||
                        this.listOptions.pagingOptions.currentPage != 1)) {
                        this.gridApi.pagination.seek(1);
                        // 将currentPage设为1之后会调用到pagingOptions的watch里面的方法，所以不需要再自己调用prototype.getPagedDataAsync()方法，否则会导致调用两次
                    }
                    else {
                        this.getPagedDataAsync();
                    }

                    this.onAfterQuery();
                };
                listController.prototype.delete = function (entity) {
                    var self = this;
                    if(!entity[self.formDataIdKey]){
                        layer.msg( "找不到要删除的数据");
                        return;
                    }
                    layer.confirm(
                        '确定删除【' + entity[self.formDataNameKey] + '】？', {
                            icon: 3,
                            btn: ['确定','取消'] //按钮
                        }, function(){//确定
                            if(self.onBeforeDelete(entity)){
                                return;
                            }
                            var deleteUrl = self.urlDelete
                                || (self.modelName + "/" + _deleteUrlSuffix + "/" + entity[self.formDataIdKey]);
                            var data = {};
                            data = self.ifJson ? data : angular.toJson(data);
                            callService.call(self.scope, null, deleteUrl, data,"get")
                                .success(function (data) {
                                    layer.msg( "删除成功");
                                    self.getPagedDataAsync();
                                    self.onAfterDelete(data);
                                })
                                .error(function (data) {

                                });
                        }, function(){//取消

                        });
                };
                /**
                 * 重置按钮的调用方法：
                 */
                listController.prototype.reset = function () {
                    this.onBeforeReset();
                    angular.copy(this.originFilterParam, this.filterParam);
                    this.query();
                    this.onAfterReset();
                };
                /**
                 * 重置按钮的调用方法：
                 */
                listController.prototype.empty = function () {
                    this.onBeforeEmpty();
                    angular.copy({}, this.filterParam);
                    //this.query();
                    this.onAfterEmpty();
                };
                /**
                 * Callback
                 */
                listController.prototype.onBeforeQuery = function () {

                };

                /**
                 * Callback
                 */
                listController.prototype.onAfterQuery = function () {
                };

                /*
                 * 删除前置操作
                 */
                listController.prototype.onBeforeDelete = function () {

                };
                /*
                 * 删除后置操作
                 */
                listController.prototype.onAfterDelete = function () {
                };
                /**
                 * Callback
                 */
                listController.prototype.onBeforeDataQueried = function (data) {

                };
                /**
                 * Callback
                 */
                listController.prototype.onAfterDataQueried = function (data) {
                };
                /**
                 * Callback
                 */
                listController.prototype.onBeforeReset = function () {

                };

                /**
                 * Callback
                 */
                listController.prototype.onAfterReset = function () {

                };
                /**
                 * Callback
                 */
                listController.prototype.onBeforeEmpty = function () {

                };

                /**
                 * Callback
                 */
                listController.prototype.onAfterEmpty = function () {

                };

                /**
                 * Callback
                 */
                listController.prototype.onAfterRegisterApi = function (gridApi) {
                };
                /*****************************************************************
                 * PRIVATE
                 ****************************************************************/
                listController.prototype.rowUnSelect = function (rowObj) {
                    var self = this;
                    if (rowObj.isSelected) {
                        return;
                    }
                    for (var j = 0; j < self.listOptions.selectedItems.length; j++) {
                        if (self.listOptions.selectedItems[j][self.formDataIdKey]
                            == rowObj.entity[self.formDataIdKey]) {
                            self.listOptions.selectedItems.splice(j, 1);
                            break;
                        }
                    }
                };
                /**
                 * 处理ui-grid中被选中与取消选中
                 * @param row 行对象
                 */
                listController.prototype.rowSelect = function () {
                    var self = this;
                    //获取当前页的被选中数据，然后对总的selectedItems进行修改
                    var selectedItems = angular.copy(self.gridApi.selection.getSelectedRows())  || [];
                    var index = -1;
                    if (!self.listOptions.multiSelect) {
                        self.listOptions.selectedItems[0] = selectedItems[0];
                        return;
                    }
                    //进行复制操作
                    for (var i = 0; i < selectedItems.length; i++) {
                        if(selectedItems[0].$$treeLevel != selectedItems[i].$$treeLevel){//树形结构表格做数据排除
                            continue;
                        }
                        for (var j = 0; j < self.listOptions.selectedItems.length; j++) {
                            if (self.listOptions.selectedItems[j][self.formDataIdKey]
                                == selectedItems[i][self.formDataIdKey]) {
                                index = j;
                                break;
                            }
                        }
                        if (index == -1) {
                            self.listOptions.selectedItems.push(selectedItems[i]);
                        } else {
                            self.listOptions.selectedItems[j] = selectedItems[i];
                        }
                    }
                };
                /**
                 * 被选中数据，叉叉按钮的onclick事件
                 * @param index
                 */
                listController.prototype.removeSelectedItems = function (index) {
                    var ctrlSelf = this;
                    var has = false;
                    var item = ctrlSelf.listOptions.selectedItems[index];
                    if (!item) {
                        return;
                    }
                    for (var i = 0; i < ctrlSelf.listOptions.data.length; i++) {
                        if(ctrlSelf.listOptions.data[0].$$treeLevel != ctrlSelf.listOptions.data[i].$$treeLevel){//树形结构表格做数据排除
                            continue;
                        }
                        if (ctrlSelf.listOptions.data[i][ctrlSelf.formDataIdKey] == item[ctrlSelf.formDataIdKey]) {
                            ctrlSelf.gridApi.selection.unSelectRow(ctrlSelf.listOptions.data[i]);
                            has = true;
                            break;
                        }
                    }
                    //找不到说明不在本页里面，直接从selectedItems里面删除
                    if (!has) {
                        ctrlSelf.listOptions.selectedItems.splice(index, 1);
                    }
                };
                listController.prototype.removeSelectedItemsAll = function (index) {
                    var ctrlSelf = this;
                    for (var i = 0; i < ctrlSelf.listOptions.data.length; i++) {
                        if(ctrlSelf.listOptions.data[0].$$treeLevel != ctrlSelf.listOptions.data[i].$$treeLevel){//树形结构表格做数据排除
                            continue;
                        }
                        ctrlSelf.gridApi.selection.clearSelectedRows();
                    }
                    ctrlSelf.listOptions.selectedItems.splice(0, ctrlSelf.listOptions.selectedItems.length);
                }
                listController.prototype.setPagingData = function (self, data, page, pageSize) {
                    var self = this;
                    if(!self.onBeforeDataQueried(data)){
                        self.gridData = data;
                        self.listOptions.data = self.gridData;
                    }
                    //获取选中行
                    setTimeout(function () {
                        for (var i = 0; i < self.listOptions.selectedItems.length; i++) {
                            for (var k = 0; k < self.listOptions.data.length; k++) {
                                if (self.listOptions.selectedItems[i][self.formDataIdKey] == self.listOptions.data[k][self.formDataIdKey]) {
                                    self.gridApi.selection.selectRow(self.listOptions.data[k]);
                                    break;
                                }
                            }
                        }
                        if (!self.scope.$$phase) {
                            self.scope.$apply();
                        }
                        self.onAfterDataQueried(data);
                    }, 100)

                };

                listController.prototype.setSumData = function (self, data, page,
                                                                    pageSize) {
                    self.sumData = data;
                    self.scope[self.listOptions.sumData] = self.sumData;
                };

                listController.prototype.getPagedDataAsync = function () {
                    //console.trace();
                    var self = this; // use 'self' to avoid 'this' points to
                    // 'window' in setTimeout
                    if (self.listOptions.filterOptions.filterText) {

                    }else {
                        var timestamp = new Date().getTime();
                        var listUrl = self.urlList
                            || (self.modelName + "/" + _listUrlSuffix );
                        var data = {};
                        if(self.requestDataFormat == "default"){
                            data =
                            {
                                currentPage : self.listOptions.pagingOptions.currentPage,
                                pageSize : self.listOptions.pagingOptions.pageSize,
                                reqBody : self.filterParam
                            };
                        }
                        if(self.requestDataFormat == "busNewFormat"){
                            data =
                            {
                                currentPage : self.listOptions.pagingOptions.currentPage,
                                pageSize : self.listOptions.pagingOptions.pageSize,
                                body : self.filterParam
                            };
                        }
                        data = self.ifJson ? data : angular.toJson(data);
                        callService.call(self.scope, null, listUrl, data, "post")
                            .success(function(response){
                                /**********************-start default*********************/
                                if(self.responseDataFormat == "default"){
                                    if(!response.resultInfo){
                                        layer.msg("返回信息格式错误");
                                        return;
                                    }
                                    if(!response.resultInfo.rows){
                                        layer.msg("返回信息格式错误");
                                        return;
                                    }
                                    if(!response.resultInfo.rows.length){
                                        layer.msg("数据库无对应数据");
                                        return;
                                    }
                                    if(self.serverPage){
                                        self.totalServerItems = response.resultInfo.total;
                                        self.listOptions.totalItems = self.totalServerItems;
                                        self.scope[self.listOptions.totalServerItems] = self.totalServerItems;
                                        self.setSumData(self, response.resultInfo.rows.length);
                                        self.setPagingData(self, response.resultInfo.rows, response.resultInfo.totalPage,
                                            response.resultInfo.pagesize);
                                    }else{
                                        self.totalServerItems = response.resultInfo.total;
                                        self.listOptions.totalItems = self.totalServerItems;
                                        self.scope[self.listOptions.totalServerItems] = self.totalServerItems;
                                        self.setSumData(self, response.resultInfo.rows.length);
                                        self.setPagingData(self, response.resultInfo.rows, response.resultInfo.totalPage,
                                            response.resultInfo.pagesize);
                                    }
                                    //jsonData.slice(firstRow, firstRow + pageSize);
                                }
                                /**********************-end default*********************/
                                /*******************-start busNewFormat*****************/
                                if(self.responseDataFormat == "busNewFormat"){  //是否是新的数据格式
                                    if(!response.data){
                                        layer.msg("返回信息格式错误");
                                        return;
                                    }else{
                                        response.data = angular.fromJson(response.data);
                                    }

                                    if(!response.data.body || !response.data.body.length){
                                        layer.msg("数据库无对应数据");
                                        response.data.body = [];
                                    }
                                    if(self.serverPage){
                                        self.totalServerItems = response.data.total;
                                        self.listOptions.totalItems = self.totalServerItems;
                                        self.scope[self.listOptions.totalServerItems] = self.totalServerItems;
                                    }else{
                                        self.totalServerItems = response.data.total;
                                        self.listOptions.totalItems = self.totalServerItems;
                                        self.scope[self.listOptions.totalServerItems] = self.totalServerItems;
                                    }
                                    if(self.fixedType == "module"){
                                        self.setSumData(self, self.fixedJson.length);
                                        self.setPagingData(self,  self.fixedJson, response.data.totalPage, response.data.pagesize);
                                    }else{
                                        self.setSumData(self, response.data.body.length);
                                        self.setPagingData(self, response.data.body, response.data.totalPage,
                                            response.data.pagesize);
                                    }
                                }
                                /*******************-end busNewFormat*****************/
                            })
                            .error(function(response){

                            });
                    }
                };


                return listController;
            }
        ]);
