if (!formModule)
    var formModule = angular.module("app.controller.form", []);
/**
 * @Title 表单公共控制器
 * @Description 表单页基本功能的封装
 * @author hedongyang
 * @date 2015-11-23
 * @version V1.0
 *
 * @Description 新增layer弹窗
 * @author hedongyang
 * @date 2016-08-14
 *
 * @Description 新增前置处理器中断能力
 * @author hedongyang
 * @date 2016-07-24
 */
formModule
    .value('formControllerOptions',
        _globalJson.config.formModuleOptions)
    .factory(
    'formController',
    [
        'formControllerOptions',
        'callService',
        'tabService',
        'initDataService',
        'confirmService',
        'alertService',
        '$state',
        'utilService',
        'modalService',
        function (formControllerOptions, callService, tabService,
                  initDataService, confirmService, alertService, $state, utilService) {

            /**
             * Creates an instance of formController
             *
             * @param {Object}
             *          [options]
             * @constructor
             */
            function formController(scope, options) {
                utilService.extendDeep(this, formControllerOptions, options);

                this.attach(scope);
            }

            /*****************************************************************
             * PUBLIC
             ****************************************************************/
            formController.prototype.attach = function (scope) {
                this.scope = scope;
                this.orginalData = angular.copy(this.formData);
                var self = this;
                scope.$watch(self.formData, function (newVal, oldVal) {
                    if (!angular.equals(newVal, self.orginalData)) {
                        self.uiControl.disBtnModify = false;
                    }else{
                        self.uiControl.disBtnModify = true;
                    }
                }, true);
                //设置按钮显示和input输入控制
                if (!self.formData.id) {
                    self.uiControl.showBtnAdd = true;
                    self.uiControl.showBtnModify = false;
                    //  console.log(" self.uiControl.showBtnModify"+ self.uiControl.showBtnModify);
                } else {
                    self.uiControl.showBtnAdd = false;
                    self.uiControl.showBtnModify = true;
                    self.uiControl.disInputsModify = true;
                }
            };

            formController.prototype.setUiControl = function (btnName , uiBoolean) {
                this.uiControl[btnName] = uiBoolean;
            };


            /**
             * 查询按钮的调用方法：
             */
            formController.prototype.load = function () {
                var self = this;
                self.onBeforeLoad(self);
                //设置按钮显示和input输入控制
                if (!self.id || self.id == 0) {
                    self.uiControl.showBtnAdd = true;
                    self.uiControl.showBtnModify = false;
                } else{
                    self.uiControl.showBtnAdd = false;
                    self.uiControl.showBtnModify = true;
                    self.uiControl.disInputsModify = true;
                    self.fetch();
                }
            };

            /**
             * 重置按钮的调用方法：
             */
            formController.prototype.reset = function () {
                this.onBeforeReset();
                angular.copy(this.orginalData, this.formData);
                this.onAfterReset();
            };
            /**
             * 返回按钮的调用方法：
             */
            formController.prototype.back = function () {
                var self = this;
                    if (self.baseUrl) {
                        $state.go(self.baseUrl);
                    } else {
                        history.back();
                    }
            };

            /**
             * Callback
             */
            //页面加载
            formController.prototype.onBeforeLoad = function () {
            };
            //获取数据
            formController.prototype.onBeforeFetch = function () {

            };
            formController.prototype.onAfterFetch = function () {
            };
            //新增
            formController.prototype.onBeforeAdd = function () {

            };

            formController.prototype.onAfterAdd = function () {
            };
            //整条更新
            formController.prototype.onBeforeUpdateWhole = function () {

            };
            formController.prototype.onAfterUpdateWhole = function () {
            };
            /*
             * 删除前置操作
             */
            formController.prototype.onBeforeDelete = function () {

            };
            /*
             * 删除后置操作
             */
            formController.prototype.onAfterDelete = function () {
            };

            //重置
            formController.prototype.onBeforeReset = function () {

            };
            formController.prototype.onAfterReset = function () {
            };

            /*****************************************************************
             * PRIVATE
             ****************************************************************/
            formController.prototype.fetch = function () {
                var self = this;
                if(self.onBeforeFetch()){
                    return;
                }
                var fetchUrl = self.modelName + "/" + _fetchUrlSuffix + "/" + self.id + "";
                return callService.call(self.scope, null, fetchUrl, null, "GET")
                    .success(function (response) {
                        angular.copy(response.resultInfo, self.formData);
                        angular.copy(response.resultInfo, self.orginalData);
                        self.onAfterFetch();
                    })
                    .error(function (response) {

                    });
            };
            //前端点击保存按钮调用
            formController.prototype.add = function () {
                var self = this;
                if(self.onBeforeAdd()){
                    return;
                }
                var saveUrl = self.modelName + "/" + _saveUrlSuffix;
                var data =
                {
                    reqBody: self.formData
                };
                callService.call(self.scope, null, saveUrl, data, "POST").success(function(data) {
                    layer.msg( "新增成功");
                    angular.copy({}, self.formData);
                    self.onAfterAdd(data);
                }).error();//提交保存请求
            };
            formController.prototype.updateWhole = function () {
                var self = this;
                layer.confirm(
                    '确定修改【' + self.formData[self.formDataNameKey] + '】？', {
                        btn: ['确定','取消'] //按钮
                    }, function(){//修改
                        if(self.onBeforeUpdateWhole()){
                            return;
                        }
                        var saveUrl = self.modelName + "/" + _updateUrlSuffix;
                        var data =
                        {
                            reqBody: self.formData
                        };
                        callService.call(self.scope, null, saveUrl, data, "POST").success(function(data) {
                            if(self.onAfterUpdateWhole(data)){
                                return;
                            }
                            layer.msg( "修改成功");
                            if (self.baseUrl && !self.ifModal) {
                                $state.go(self.baseUrl);
                            }
                        }).error();//提交保存请求
                    }, function(){//取消

                    });

            };
            formController.prototype.delete = function () {
                var self = this;
                layer.confirm(
                    '确定删除【' + self.formData[self.formDataNameKey] + '】？', {
                        btn: ['确定','取消'] //按钮
                    }, function(){//删除
                        if(self.onBeforeDelete()){
                            return;
                        }
                        var listUrl = self.modelName + "/" + _deleteUrlSuffix + "/" + self.formData[self.formDataIdKey];
                        var data = {};
                        callService.call(self.scope, null, listUrl, data,"GET")
                            .success(function (data) {
                                if (self.baseUrl && !self.ifModal) {
                                    $state.go(self.baseUrl);
                                }
                                layer.msg( "删除成功！");
                                self.onAfterDelete(data);
                            })
                            .error(function (data) {

                            });
                    }, function(){//取消

                    });
            };
            return formController;
        }
    ]);
