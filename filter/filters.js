/**
 * 后台int数值对应于 前端中文名称显示过滤器（数据源是data/global.json）
 */
if (!filterModule)
    var filterModule = angular.module("app.filter", []);
filterModule.filter("typeLabelFilter",
    [
        '$rootScope',
        function() {
          return function(input, type1) {
            try {
                if (!input && input !== 0) {
                return '';
              }
              if (!type1) {
                return '';
              }
              var resultStr = "";
              var prefix = "";
                var arr = [];
              if (isNaN(input)) {
                arr = input.split(",");
              }else {
                arr.push(input + "");
              }
              for (var k = 0; k < arr.length; k++) {
                  if (arr[k] || arr[k] === 0) {
                	for ( var temp in _globalJson) {
                        if (angular.equals(temp, type1)) {
                          var _array = _globalJson[temp];// 到此，是一个数组
                          for (var j = 0; j < _array.length; j++) {
                            var label_value = _array[j]; // 此对象必须是{"label":"_label","value":"_value"}
                            var tmpArr = arr[k].split("-");
                              if (!tmpArr[0] && tmpArr[0] !== 0) {
                                  tmpArr[0] = arr[k];
                              }
                            if (tmpArr[0] == label_value.value &&
                                label_value.label) {
                              resultStr += prefix + label_value.label;
                              if (prefix == "")
                                prefix = ",";
                            }
                          }
                        }
                      }
                }
              }
              return resultStr;
            }
            catch (e) {
              console.warn("获取 label:" + type1 + " 失败...");
              console.error(e);
            }
            return '';
          };
        }
    ]);
/**
 * 数据替换
 * 如果input等于before则替换为after
 * 如果ifInput有数据则替换为ifInput
 */
filterModule.filter("replaceFilter",
    [
        '$rootScope',
        function() {
            return function(input,before,after,ifInput) {
                if (!input) {
                    input = "";
                }
                if (!after) {
                    after = input;
                }
                if(ifInput){
                    if(ifInput == before){
                        return after;
                    }
                }else{
                    if(input == before){
                        return after;
                    }
                }
                return input;
            };
        }
    ]);
/**
 * 在一个字符串的某个位置加上另一个字符串
 */
filterModule.filter("concatStringFilter",
    [
        function() {
            return function(input,content,startIndex) {
                if (!input)
                    return '';
                if (!content)
                    content = '';
                if (!startIndex && startIndex !== 0)
                    startIndex = -1;//默认加在最后
                if (typeof input !== 'string')
                    return input;
                if (typeof startIndex !== 'number')
                    return input;

                var result = '';
                if(startIndex == 0){//加在input之前
                    result = content + input;
                }else if(startIndex == -1){//加在input之后
                    result = input + content;
                }else{
                    var str1 = input.substring(0,startIndex);
                    var str2 = input.substring(startIndex,input.length);
                    result = str1 + content + str2;
                }
                return result;
            };
        }
    ]);
/**
 * 限制小数位数
 * length——小数位数
 */
filterModule.filter("fixedFilter", function() {
	return function(input,length) {
		try {
			if (!input) {
				return '';
			}
			if (typeof input === 'number') {
				return input.toFixed(length);
			}
		}
		catch (e) {
			console.error("限制小数位数Filter:" + input + " 失败...");
			console.error(e);
		}
		return '';
	};
});
/**
 * 小数转化成百分数
 * length——转化成百分数之后保留几位小数
 */
filterModule.filter("percentageFilter", function() {
	return function(input,length) {
		try {
			if (!input) {
				return '';
			}
			if (typeof input === 'number') {
				return (input*100).toFixed(length)+'%';
			}
		}
		catch (e) {
			console.error("小数转化成百分数Filter:" + input + " 失败...");
			console.error(e);
		}
		return '';
	};
});

/**
 * 分转化为元
 */
filterModule.filter("priceFilter", function() {
  return function(input) {
    try {
      if (!input) {
        return '';
      }
      if (!isNaN(input)) {
        var price = input / 100.0;
        return price;
      }

    }
    catch (e) {
      console.error("获取 price:" + input + " 失败...");
      console.error(e);
    }
    return '';
  };
});

/**
 * zone_id/zone_ids转化为zone_name/zone_names
 */
filterModule.filter("zoneIdFilter", [
            'initDataService',
            function(initDataService) {
              return function(input) {
                try {
                  if (!input) {
                    return '';
                  }

                  var resultStr = "";
                    var arr = [];
                  if (isNaN(input)) {
                    arr = input.split(",");
                  }
                  else {
                    arr.push(input);
                  }

                  var region = initDataService["region"];
                  var zone = {};
                  if (region) {
                    for (var k = 0; k < arr.length; k++) {
                      if (arr[k]) {
                        var zone_id = arr[k];
                        zone.province = "" + parseInt(zone_id / 10000) * 10000;
                        zone.zone_name = region[zone.province].zone_name || "";

                        if (zone_id % 10000) {
                          zone.city = "" + parseInt(zone_id / 100) * 100;
                          zone.zone_name = region[zone.province].subs[zone.city].zone_name ||
                                           "";
                        }

                        if (zone_id % 100) {
                          zone.district = "" + zone_id;
                          zone.zone_name = region[zone.province].subs[zone.city].subs[zone.district].zone_name ||
                                           "";
                        }

                        if (zone.zone_name || zone.zone_name === 0) {
                          if (resultStr) {
                            resultStr += "," + zone.zone_name;
                          }
                          else {
                            resultStr += zone.zone_name;
                          }
                        }
                      }
                    }
                  }
                  
                  return resultStr;
                }
                catch (e) {
                  console.error("获取 zone:" + input + " 失败...");
                  console.error(e);
                }
                return '';
              };
            }
        ]);

/**
 * 根据用逗号连接的id字符串得到相应的用逗号连接的name字符串,或根据单个id获取相应的name（数据源是数据库，且该表数据量很小的时候使用。数据源是从数据库中单表直接获取,存在initDataService中的对象中）
 * 传入type值，type是initDataService中的对象名称
 * before_param——需要被转化的字段名，如果没有传，则默认before_param = type+"_id"
 * after_params——目标字段的字段名，如果有多个，则用逗号连接；如果没有传，则默认after_params=“name”。
 * 连接符connector如果没传，则默认是“-”,例：after_params若等于“name,code”,那么返回值为“name-code,name-code,name-code,name-code”
 */
filterModule.filter("paramTransferFilter", [
    'initDataService',
    function(initDataService) {
      return function(input, type, before_param, after_params, connector) {// 修改relations数据结构之后before_param已经没有用处
        try {

          if (!input) {
            return '';
          }
          if (!type) {
            return '';
          }
          if (!connector) {
            connector = "-";
          }
          if (!before_param) {
            before_param = type + "_id";
          }
          if (!after_params) {
            after_params = "name";
          }
          var resultStr = "";
            var arr = [];
          if (isNaN(input)) {
            arr = input.split(",");
          }
          else {
            arr.push(input);
          }

            var paramArr = [];
          paramArr = after_params.split(",");

          if (initDataService[type]) {
            for (var k = 0; k < arr.length; k++) {
              if (arr[k]) {
                var hasData = false;
                for (var i = 0; i < initDataService[type].length; i++) {
                  if (initDataService[type][i]) {
                    if (initDataService[type][i][before_param] == arr[k]) {
                      hasData = true;
                      // 拼接多个字段的值
                      var str = "";
                      for (var j = 0; j < paramArr.length; j++) {
                        if (j == paramArr.length - 1) {
                          if (initDataService[type][i][paramArr[j]] ||
                              initDataService[type][i][paramArr[j]] === 0) {
                            str += initDataService[type][i][paramArr[j]];
                          }
                        }
                        else {
                          if (initDataService[type][i][paramArr[j]] ||
                              initDataService[type][i][paramArr[j]] === 0) {
                            str += initDataService[type][i][paramArr[j]] +
                                   connector;
                          }
                        }
                      }
                      if (str || str === 0) {
                        if (resultStr) {
                          resultStr += "," + str;
                        }
                        else {
                          resultStr += str;
                        }
                      }
                      break;
                    }
                  }
                  // else {
                  // if (k == arr.length - 1) {
                  // resultStr += arr[k];
                  // }else {
                  // resultStr += arr[k] + ",";
                  // }
                  // break;
                  // }
                }
                // if (!hasData)
                // {
                // if (k == arr.length - 1) {
                // resultStr += arr[k];
                // }else {
                // resultStr += arr[k] + ",";
                // }
                // }
              }
            }
          }
          // else {
          // return input;
          // }
          return resultStr;
        }
        catch (e) {
          console.error("获取属性:" + type + " 失败...");
          console.error(e);
        }
        return '';
      };
    }
]);

/**
 * 根据用逗号连接的param0字符串得到相应的用逗号连接的Param2字符串,或根据单个param0获取相应的Param2（数据源是页面获取data时带回的relations）
 * 传入type值，type是relations中的对象名称(需要与别表关联的字段的名称，例如：tbl_task_wms表中worker_storage_ids字段需要与tbl_worker进行关联，此时type就是“worker_storage_ids”)
 * before_param——需要被转化的字段名，如果没有传，则默认before_param = type
 * after_params——目标字段的字段名，如果有多个，则用逗号连接；如果没有传，则默认after_params=“name”。
 * 连接符connector如果没传，则默认是“-”,例：after_params若等于“name,code”,那么返回值为“name-code,name-code,name-code,name-code”
 * symbol——“,”转换后的字符默认为“，”
 */
filterModule.filter("paramTransferFilterByRelations", [
        'initDataService', '$filter',
        function (initDataService, $filter) {
            return function (input, type, before_param, after_params,
                             connector, hasFilter,symbol) {// 改变relations的数据结构之后，before_param已失去作用
                try {
                    if (!input)
                        return '';
                    if (!type)
                        return '';
                    if (!before_param)
                        before_param = type;
                    if (!after_params)
                        after_params = "name";
                    if (!connector)
                        connector = "-";
                    if (!symbol)
                        symbol = "，";
                    var prePath = "";
                    if(type == "basic_package_id" || before_param == "basic_package_id"|| after_params == "alias_name"){
                        console.dir(input);
                    }
                    var indexField = type;
                    if (type.indexOf('/') > -1 && !hasFilter) {
                        var fields = type.split('/');
                        for (var i = 1, size = fields.length; i < size; i++) {
                            var preType = fields[0];
                            for (var j = 1; j < i; j++)
                                preType += "/" + fields[j];
                            var preParam = fields[i];
                            input = $filter('paramTransferFilterByRelations')(input, preType, null, preParam, null, true);
                            prePath = preType + "/" + preParam + "/";
                        }
                        indexField = fields[fields.length - 1];
                    }

                    var inputArr = [];
                    if (typeof input === "object") {
                        var indexFields = indexField.split('|');
                        var indexPath = '';
                        for (var i = 0; i < indexFields.length; i++) {
                            if (i % 2 == 0)
                                indexPath += indexFields[i];
                            else
                                indexPath += input[indexFields[i]];
                        }
                        type = prePath + indexPath;

                        var indexFieldStr = indexField.substr(0, indexField.indexOf('#'));
                        var fieldss = indexFieldStr.split(',');
                        var valueObject = {"|": 1};
                        for (var j = 0; j < fieldss.length; j++) {
                            for (var key in valueObject) {
                                delete valueObject[key];
                                var fieldValue = input[fieldss[j]];
                                if (isNaN(fieldValue)) {
                                    var values = fieldValue.split(",");
                                    for (var k = 0; k < values.length; k++) {
                                        valueObject[key + values[k] + "|"] = 1;
                                    }
                                }
                                else {
                                    valueObject[key + fieldValue + "|"] = 1;
                                }
                            }
                        }
                        for (var key in valueObject) {
                            delete valueObject[key];
                            inputArr.push(key);
                        }
                    }
                    else if (isNaN(input))
                        inputArr = input.split(",");
                    else
                        inputArr.push(input);
                    if (!initDataService.relations[type])
                        return '';

                    var paramArr = [];
                    paramArr = after_params.split(",");

                    var resultStr = "";
                    for (var k = 0; k < inputArr.length; k++) {
                        if (!inputArr[k])
                            continue;
                        if (!initDataService.relations[type]['' + inputArr[k]])
                            continue;
                        var str = "";
                        for (var j = 0; j < paramArr.length; j++) {
                            if (!paramArr[j])
                                continue;
                            var value = initDataService.relations[type]['' + inputArr[k]][paramArr[j]];
                            if (!value && !angular.equals(value, 0))
                                continue;
                            if (j < paramArr.length - 1){
                                //if(str != value + connector)
                                    str += value + connector;
                            }else{
                                //if(str != value )
                                    str += value;
                            }
                        }

                        if (!str && str !== 0)
                            continue;
                        if ((symbol + resultStr + symbol).indexOf(',' + str + symbol) > -1)
                            continue;

                        if (resultStr)
                            resultStr += symbol + str;
                        else
                            resultStr += str;

                    }
                    // else {
                    // return input;
                    // }
                    return resultStr;
                } catch (e) {
                    console.error("获取 paramTransferFilterByRelations:" + type + " 失败...");
                    console.error(e);
                    return '';
                }
            };
        }
    ]);
/**
 * 专门用于生成固定的单位形式：例：“1箱 = 12包” 传入type值，type是relations中的对象名称，不传则默认type = "good_id"
 * before_param——需要被转化的字段名，如果没有传，则默认before_param = "good_id"
 */
filterModule.filter("unitFilterByRelations", [
            'initDataService',
            function(initDataService) {
              return function(input, type, before_param) {// 改变relations的数据结构之后，before_param已失去作用
                try {
                  if (!input) {
                    return '';
                  }

                  if (!type) {
                    type = "good_id";
                  }
                  if (!before_param) {
                    before_param = "good_id";
                  }

                  var resultStr = "";
                    var inputArr = [];
                  if (isNaN(input)) {
                    inputArr = input.split(",");
                  }
                  else {
                    inputArr.push(input);
                  }

                  if (initDataService.relations[type]) {
                    for (var k = 0; k < inputArr.length; k++) {

                      if (inputArr[k]) {

                        if (initDataService.relations[type]['' + inputArr[k]]) {
                          var tray_cell_unit = initDataService.relations[type]['' +
                                                                               inputArr[k]]["tray_cell_unit"];
                          var inner_packing_number = initDataService.relations[type]['' +
                                                                                     inputArr[k]]["inner_packing_number"];
                          var inner_packing_unit = initDataService.relations[type]['' +
                                                                                   inputArr[k]]["inner_packing_unit"];

                          if (inner_packing_unit) {
                            if (resultStr) {

                              if (inner_packing_number &&
                                  inner_packing_number > 1) {
                                resultStr += ',' + "1 " + tray_cell_unit +
                                             " = " + inner_packing_number +
                                             inner_packing_unit;
                              }
                              else {
                                resultStr += ',' + tray_cell_unit;
                              }
                            }
                            else {
                              if (inner_packing_number &&
                                  inner_packing_number > 1) {
                                resultStr += "1 " + tray_cell_unit + " = " +
                                             inner_packing_number +
                                             inner_packing_unit;
                              }
                              else {
                                resultStr += tray_cell_unit;
                              }
                            }
                          }
                          else {
                            if (tray_cell_unit) {
                              if (resultStr) {
                                resultStr += ',' + tray_cell_unit;
                              } else {
                                resultStr += tray_cell_unit;
                              }
                            }
                          }
                        }
                        // else {
                        // if (k == inputArr.length - 1) {
                        // resultStr += inputArr[k];
                        // }else {
                        // resultStr += inputArr[k] + ',';
                        // }
                        // }
                      }
                    }
                  }
                  // else {
                  // return input;
                  // }

                  return resultStr;
                }
                catch (e) {
                  console.error("获取 unitFilterByRelations:" + type + " 失败...");
                  console.error(e);
                }
                return '';
              };
            }
        ]);
/**
 * paramsLinkedWithCommaFilter
 * 根据数组，返回一个用逗号连接的所有param字段的字符串
 */
filterModule.filter("paramsLinkedWithCommaFilter", function() {
  return function(data, param) {
    try {
      if (!data) {
        return '';
      }
      if (data.length == 0) {
        return '';
      }
      if (!param) {
        return '';
      }
      var resultStr = ",";
      for (var i = 0; i < data.length; i++)
        if (data[i]) {
          if (data[i][param]) {
            // if (i == data.length - 1) {
            // resultStr += data[i][param];
            // }else {
            // }
            resultStr += data[i][param] + ",";
          }
        }
      return resultStr;
    }
    catch (e) {
      console.error("获取paramsLinkedWithCommaFilter:" + data + " 失败...");
      console.error(e);
    }
    return '';
  };
});

/**
 * !!!!!此filter已废弃，改用getBeansWithParamFilter！！
 * 根据用逗号连接的id字符串得到所有id对应的数据对象,或者根据单个id获得相应的数据对象（数据源是数据库时使用）
 * 传入type值，type就是该表的主键id名，比如tbl_worker表，type就是worker_id
 */
filterModule.filter("getBeansWithIdsFilter", [
    'initDataService', function(initDataService) {
      return function(input, type) {
        try {
          if (!input) {
            return '';
          }
          if (!type) {
            return '';
          }
            var resultArr = [];
            var arr = [];
          if (isNaN(input)) {
            arr = input.split(",");
          }
          else {
            arr.push(input);
          }
          if (initDataService.relations[type]) {
            for (var k = 0; k < arr.length; k++) {
              if (arr[k]) {

                if (initDataService.relations[type]['' + arr[k]]) {
                  resultArr.push(initDataService.relations[type]['' + arr[k]]);
                }
                else {
                  resultArr.push(
                  {
                    type : input
                  });
                }
              }
            }
          }
          // else {
          // return input;
          // }
          return resultArr;
        }
        catch (e) {
          console.error("获取 getBeansWithIdsFilter:" + type + " 失败...");
          console.error(e);
        }
        return '';
      };
    }
]);

/**
 * getBeansWithParamFilter 是 getBeansWithIdsFilter的扩展：
 * 根据某个字段的值得到对应的数据对象（如果该字段有多个值，则用逗号连接）
 * 传入type值，type是relations中的对象名称(需要与别表关联的字段的名称，例如：tbl_task_wms表中worker_storage_ids字段需要与tbl_worker进行关联，此时type就是“worker_storage_ids”)
 * param——字段名。如果没有传，则默认param = type
 */
filterModule.filter("getBeansWithParamFilter", [
    'initDataService', function(initDataService) {
      return function(input, type, param) {
        try {
          if (!input) {
            return '';
          }
          if (!type) {
            return '';
          }
          if (!param) {
            return '';
          }
            var resultArr = [];
            var arr = [];
          if (isNaN(input)) {
            arr = input.split(",");
          }
          else {
            arr.push(input);
          }
          if (!param) {
            param = type;
          }
          if (initDataService.relations[type]) {
            for (var k = 0; k < arr.length; k++) {
              if (arr[k]) {

                if (initDataService.relations[type]['' + arr[k]]) {
                  resultArr.push(initDataService.relations[type]['' + arr[k]]);
                }
                else {
                  resultArr.push(
                  {
                    param : input
                  });
                }
              }
            }
          }
          // else {
          // return input;
          // }

          return resultArr;
        }
        catch (e) {
          console.error("获取 getBeansWithParamFilter:" + type + " 失败...");
          console.error(e);
        }
        return '';
      };
    }
]);

/**
 * getBeansWithParamFilter 是 getBeansWithIdsFilter的扩展：
 * 根据某个字段的值得到对应的数据对象（如果该字段有多个值，则用逗号连接）
 * 传入type值，type是relations中的对象名称(需要与别表关联的字段的名称，例如：tbl_task_wms表中worker_storage_ids字段需要与tbl_worker进行关联，此时type就是“worker_storage_ids”)
 * param——字段名。如果没有传，则默认param = type
 */
filterModule.filter("trueOrFalseFilter", [
    'initDataService', function() {
      return function(input) {

        if (input == true) {

          return "是";

        }
        else {

          return "否";
        }

      };
    }
]);
/**
 *字符串转化成数组后获取其中某个指定索引的值
 */
filterModule.filter("selectValueFilter",
    [
        function() {
            return function(input,index,connector) {
                if (!input)
                    return '';
                if (typeof index !== 'number')
                    index=0;
                if (typeof connector !== 'string')
                    connector=',';

                return input.split(connector)[index];
            };
        }
    ]);

/**
 * 字符串转化成数组
 */
filterModule.filter("stringToArrayFilter",
    [
        function() {
            return function(input,connector,isRemoveBlank) {
                if (!input)
                    return '';
                if (typeof connector !== 'string')
                    connector=',';
                if(isRemoveBlank == null || isRemoveBlank == undefined){
                    isRemoveBlank = true;
                }
                var array = input.split(connector) || [];
                if(isRemoveBlank){
                    for (var k = 0; k < array.length; k++) {
                        if(!array[k] && array[k] !== 0){
                            array.splice(k,1);
                            k--;
                        }
                    }
                }
                return array;
            };
        }
    ]);
/**
 * 根据字段来判断是否显示%   价格策略
 */
filterModule.filter("priceStrategyFilter", function () {
    return function (input, type) {
        try {            //(input*100).toFixed(length)+'%';
            if (!input) {
                return '';
            }
            var showLow = "";
            if (input.strategy_item == 1 || input.strategy_item == 3) {
                if (type == "upper_limit") {
                    if (input.upper_limit != undefined && input.upper_limit != 0) {
                        showLow = (input.upper_limit * 100).toFixed(2) + '%'
                    } else {
                        showLow = ""
                    }
                }
                if (type == "lower_limit") {
                    if (input.lower_limit != undefined && input.lower_limit != 0) {
                        showLow = (input.lower_limit * 100).toFixed(2) + '%'
                    } else {
                        showLow = ""
                    }
                }
            } else if(input.strategy_item == 2 || input.strategy_item == 4){
                if (type == "upper_limit") {
                    if (input.upper_limit != undefined && input.upper_limit != 0) {
                        showLow = (input.upper_limit).toFixed(2) + '元'
                    } else {
                        showLow = ""
                    }
                }
                if (type == "lower_limit") {
                    if (input.lower_limit != undefined && input.lower_limit != 0) {
                        showLow = (input.lower_limit).toFixed(2) + '元'
                    } else {
                        showLow = ""
                    }
                }
            }
            //  console.log(input)
            return showLow;
        }
        catch (e) {
            console.error("小数转化成百分数Filter:" + input + " 失败...");
            console.error(e);
        }
        return '';
    };
});



